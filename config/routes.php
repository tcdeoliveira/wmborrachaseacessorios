<?php
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
Router::defaultRouteClass(DashedRoute::class);
Router::scope('/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
    $routes->connect('/quem-somos', ['controller' => 'Pages', 'action' => 'display', 'sobre']);
    $routes->connect('/wmacesso', ['controller' => 'Pages', 'action' => 'display', 'login']);
    $routes->connect('/admcp/categorias', ['controller' => 'ProdutoCategorias', 'prefix'=>'admcp', 'action' => 'index']);
    $routes->connect('/admcp/categorias/editar/*', ['controller' => 'ProdutoCategorias', 'prefix'=>'admcp', 'action' => 'edit']);
    $routes->connect('/admcp/categorias/nova', ['controller' => 'ProdutoCategorias', 'prefix'=>'admcp', 'action' => 'add']);
    $routes->connect('/admcp/produtos/novo', ['controller' => 'Produtos', 'prefix'=>'admcp', 'action' => 'add']);
    $routes->connect('/admcp/produtos/editar/*', ['controller' => 'Produtos', 'prefix'=>'admcp', 'action' => 'add']);
    $routes->connect('/search/*', ['controller' => 'Produtos', 'action' => 'search']);
    $routes->connect('/admcp/produtos/favoritar/*', ['controller' => 'Produtos', 'prefix'=>'admcp', 'action' => 'favoritar']);
    $routes->connect('/admcp/usuarios/novo', ['controller' => 'Users', 'prefix'=>'admcp', 'action' => 'add']);

    $routes->connect('/admcp/usuarios/', ['controller' => 'Users', 'prefix'=>'admcp', 'action' => 'index']);
    $routes->connect('/admcp/usuarios/editar/*', ['controller' => 'Users', 'prefix'=>'admcp', 'action' => 'index']);
    $routes->connect('/produtos/detalhes/*', ['controller' => 'Produtos', 'action' => 'view']);
    $routes->connect('/admcp/login', ['controller' => 'Users', 'prefix'=>'admcp', 'action' => 'login']);
        $routes->connect('/admcp/sair', ['controller' => 'Users', 'prefix'=>'admcp', 'action' => 'logout']);

    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
    $routes->fallbacks(DashedRoute::class);
});
Plugin::routes();
Router::prefix('admcp', function($routes){
			$routes->connect('/', ['controller' => 'Users', 'action' => 'index']);
    			$routes->fallbacks('DashedRoute');
});

