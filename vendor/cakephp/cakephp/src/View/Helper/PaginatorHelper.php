<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         1.2.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
namespace Cake\View\Helper;

use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;
use Cake\View\View;

/**
 * Pagination Helper class for easy generation of pagination links.
 *
 * PaginationHelper encloses all methods needed when working with pagination.
 *
 * @property \Cake\View\Helper\UrlHelper $Url
 * @property \Cake\View\Helper\NumberHelper $Number
 * @property \Cake\View\Helper\HtmlHelper $Html
 * @property \Cake\View\Helper\FormHelper $Form
 * @link https://book.cakephp.org/3.0/en/views/helpers/paginator.html
 */
class PaginatorHelper extends Helper
{

    use StringTemplateTrait;

    /**
     * List of helpers used by this helper
     *
     * @var array
     */
    public $helpers = ['Url', 'Number', 'Html', 'Form'];

    /**
     * Default config for this class
     *
     * Options: Holds the default options for pagination links
     *
     * The values that may be specified are:
     *
     * - `url` Url of the action. See Router::url()
     * - `url['sort']` the key that the recordset is sorted.
     * - `url['direction']` Direction of the sorting (default: 'asc').
     * - `url['page']` Page number to use in links.
     * - `model` The name of the model.
     * - `escape` Defines if the title field for the link should be escaped (default: true).
     *
     * Templates: the templates used by this class
     *
     * @var array
     */
    protected $_defaultConfig = [
        'options' => [],
        'templates' => [
            'nextActive' => '<li class="page-item  pganinator-item-ibhj"><a class="page-link page-link-ibhj" rel="next" href="{{url}}">{{text}}</a></li>',
            'nextDisabled' => '<li class="page-item  pganinator-item-ibhj disabled "><a class="page-link page-link-ibhj-disabled" href="" onclick="return false;">{{text}}</a></li>',
            'prevActive' => '<li class="page-item  pganinator-item-ibhj"><a class="page-link page-link-ibhj" rel="prev" href="{{url}}">{{text}}</a></li>',
            'prevDisabled' => '<li class="page-item  pganinator-item-ibhj disabled"><a class="page-link page-link-ibhj-disabled" href="" onclick="return false;">{{text}}</a></li>',
            'counterRange' => '{{start}} - {{end}} of {{count}}',
            'counterPages' => '{{page}} of {{pages}}',
            'first' => '<li class="first"><a class="page-link page-link-ibhj" href="{{url}}">{{text}}</a></li>',
            'last' => '<li class="last"><a class="page-link page-link-ibhj" href="{{url}}">{{text}}</a></li>',
            'number' => '<li><a class="page-link page-link-ibhj " href="{{url}}">{{text}}</a></li>',
            'current' => '<li class="active"><a class="page-link page-link-ibhj  border-cor-ibhj page-link-ibhj-active "  href="">{{text}}</a></li>',
            'ellipsis' => '<li class="ellipsis">&hellip;</li>',
            'sort' => '<a class="ibhj-sort" href="{{url}}">{{text}}</a>',
            'sortAsc' => '<a class="ibhj-sort ibhj-sort-asc " href="{{url}}">{{text}}<i class="material-icons">&#xE8E5;</i></a>',
            'sortDesc' => '<a class="ibhj-sort ibhj-sort-desc" href="{{url}}">{{text}} <i class="material-icons">&#xE8E3;</i></a>',
            'sortAscLocked' => '<a class="ibhj-sort-asc-locked" href="{{url}}">{{text}}</a>',
            'sortDescLocked' => '<a class="ibhj-sort-asc-locked" href="{{url}}">{{text}}</a>',
        ]
    ];

    /**
     * Default model of the paged sets
     *
     * @var string
     */
    protected $_defaultModel;

    /**
     * Constructor. Overridden to merge passed args with URL options.
     *
     * @param \Cake\View\View $View The View this helper is being attached to.
     * @param array $config Configuration settings for the helper.
     */
    public function __construct(View $View, array $config = [])
    {
        parent::__construct($View, $config);

        $query = $this->request->getQueryParams();
        unset($query['page'], $query['limit'], $query['sort'], $query['direction']);
        $this->setConfig(
            'options.url',
            array_merge($this->request->getParam('pass'), ['?' => $query])
        );
    }
    public function params($model = null)
    {
        if (empty($model)) {
            $model = $this->defaultModel();
        }
        if (!$this->request->getParam('paging') || !$this->request->getParam('paging.' . $model)) {
            return [];
        }

        return $this->request->getParam('paging.' . $model);
    }
    public function param($key, $model = null)
    {
        $params = $this->params($model);
        if (!isset($params[$key])) {
            return null;
        }

        return $params[$key];
    }
    public function options(array $options = [])
    {
        if (!empty($options['paging'])) {
            if (!$this->request->getParam('paging')) {
                $this->request->params['paging'] = [];
            }
            $this->request->params['paging'] = $options['paging'] + $this->request->getParam('paging');
            unset($options['paging']);
        }
        $model = $this->defaultModel();

        if (!empty($options[$model])) {
            if (!$this->request->getParam('paging.' . $model)) {
                $this->request->params['paging'][$model] = [];
            }
            $this->request->params['paging'][$model] = $options[$model] + $this->request->getParam('paging.' . $model);
            unset($options[$model]);
        }
        $this->_config['options'] = array_filter($options + $this->_config['options']);
        if (empty($this->_config['options']['url'])) {
            $this->_config['options']['url'] = [];
        }
        if (!empty($this->_config['options']['model'])) {
            $this->defaultModel($this->_config['options']['model']);
        }
    }
    public function current($model = null)
    {
        $params = $this->params($model);

        if (isset($params['page'])) {
            return $params['page'];
        }

        return 1;
    }
    public function total($model = null)
    {
        $params = $this->params($model);

        if (isset($params['pageCount'])) {
            return $params['pageCount'];
        }

        return 0;
    }
    public function sortKey($model = null, array $options = [])
    {
        if (empty($options)) {
            $options = $this->params($model);
        }
        if (!empty($options['sort'])) {
            return $options['sort'];
        }

        return null;
    }
    public function sortDir($model = null, array $options = [])
    {
        $dir = null;

        if (empty($options)) {
            $options = $this->params($model);
        }

        if (isset($options['direction'])) {
            $dir = strtolower($options['direction']);
        }

        if ($dir === 'desc') {
            return 'desc';
        }

        return 'asc';
    }
    protected function _toggledLink($text, $enabled, $options, $templates)
    {
        $template = $templates['active'];
        if (!$enabled) {
            $text = $options['disabledTitle'];
            $template = $templates['disabled'];
        }

        if (!$enabled && $text === false) {
            return '';
        }
        $text = $options['escape'] ? h($text) : $text;

        $templater = $this->templater();
        $newTemplates = !empty($options['templates']) ? $options['templates'] : false;
        if ($newTemplates) {
            $templater->push();
            $templateMethod = is_string($options['templates']) ? 'load' : 'add';
            $templater->{$templateMethod}($options['templates']);
        }

        if (!$enabled) {
            $out = $templater->format($template, [
                'text' => $text,
            ]);

            if ($newTemplates) {
                $templater->pop();
            }

            return $out;
        }
        $paging = $this->params($options['model']);

        $url = array_merge(
            $options['url'],
            ['page' => $paging['page'] + $options['step']]
        );
        $url = $this->generateUrl($url, $options['model']);

        $out = $templater->format($template, [
            'url' => $url,
            'text' => $text,
        ]);

        if ($newTemplates) {
            $templater->pop();
        }

        return $out;
    }
    public function prev($title = '<< Previous', array $options = [])
    {
        $defaults = [
            'url' => [],
            'model' => $this->defaultModel(),
            'disabledTitle' => $title,
            'escape' => true,
        ];
        $options += $defaults;
        $options['step'] = -1;

        $enabled = $this->hasPrev($options['model']);
        $templates = [
            'active' => 'prevActive',
            'disabled' => 'prevDisabled'
        ];

        return $this->_toggledLink($title, $enabled, $options, $templates);
    }
    public function next($title = 'Next >>', array $options = [])
    {
        $defaults = [
            'url' => [],
            'model' => $this->defaultModel(),
            'disabledTitle' => $title,
            'escape' => true,
        ];
        $options += $defaults;
        $options['step'] = 1;

        $enabled = $this->hasNext($options['model']);
        $templates = [
            'active' => 'nextActive',
            'disabled' => 'nextDisabled'
        ];

        return $this->_toggledLink($title, $enabled, $options, $templates);
    }
    public function sort($key, $title = null, array $options = [])
    {
        $options += ['url' => [], 'model' => null, 'escape' => true];
        $url = $options['url'];
        unset($options['url']);

        if (empty($title)) {
            $title = $key;

            if (strpos($title, '.') !== false) {
                $title = str_replace('.', ' ', $title);
            }

            $title = __(Inflector::humanize(preg_replace('/_id$/', '', $title)));
        }
        $defaultDir = isset($options['direction']) ? strtolower($options['direction']) : 'asc';
        unset($options['direction']);

        $locked = isset($options['lock']) ? $options['lock'] : false;
        unset($options['lock']);

        $sortKey = $this->sortKey($options['model']);
        $defaultModel = $this->defaultModel();
        $model = $options['model'] ?: $defaultModel;
        list($table, $field) = explode('.', $key . '.');
        if (!$field) {
            $field = $table;
            $table = $model;
        }
        $isSorted = (
            $sortKey === $table . '.' . $field ||
            $sortKey === $model . '.' . $key ||
            $table . '.' . $field === $model . '.' . $sortKey
        );

        $template = 'sort';
        $dir = $defaultDir;
        if ($isSorted) {
            if ($locked) {
                $template = $dir === 'asc' ? 'sortDescLocked' : 'sortAscLocked';
            } else {
                $dir = $this->sortDir($options['model']) === 'asc' ? 'desc' : 'asc';
                $template = $dir === 'asc' ? 'sortDesc' : 'sortAsc';
            }
        }
        if (is_array($title) && array_key_exists($dir, $title)) {
            $title = $title[$dir];
        }

        $url = array_merge(
            ['sort' => $key, 'direction' => $dir],
            $url,
            ['order' => null]
        );
        $vars = [
            'text' => $options['escape'] ? h($title) : $title,
            'url' => $this->generateUrl($url, $options['model']),
        ];

        return $this->templater()->format($template, $vars);
    }
    public function generateUrl(array $options = [], $model = null, $urlOptions = false)
    {
        if (!is_array($urlOptions)) {
            $urlOptions = ['fullBase' => $urlOptions];
        }
        $urlOptions += [
            'escape' => true,
            'fullBase' => false
        ];

        return $this->Url->build($this->generateUrlParams($options, $model), $urlOptions);
    }
    public function generateUrlParams(array $options = [], $model = null)
    {
        $paging = $this->params($model);
        $paging += ['page' => null, 'sort' => null, 'direction' => null, 'limit' => null];

        $url = [
            'page' => $paging['page'],
            'limit' => $paging['limit'],
            'sort' => $paging['sort'],
            'direction' => $paging['direction'],
        ];

        if (!empty($this->_config['options']['url'])) {
            $key = implode('.', array_filter(['options.url', Hash::get($paging, 'scope', null)]));
            $url = array_merge($url, Hash::get($this->_config, $key, []));
        }

        $url = array_filter($url, function ($value) {
            return ($value || is_numeric($value));
        });
        $url = array_merge($url, $options);

        if (!empty($url['page']) && $url['page'] == 1) {
            $url['page'] = false;
        }
        if (isset($paging['sortDefault'], $paging['directionDefault'], $url['sort'], $url['direction']) &&
            $url['sort'] === $paging['sortDefault'] &&
            $url['direction'] === $paging['directionDefault']
        ) {
            $url['sort'] = $url['direction'] = null;
        }

        if (!empty($paging['scope'])) {
            $scope = $paging['scope'];
            $currentParams = $this->_config['options']['url'];
            // Merge existing query parameters in the scope.
            if (isset($currentParams['?'][$scope]) && is_array($currentParams['?'][$scope])) {
                $url += $currentParams['?'][$scope];
                unset($currentParams['?'][$scope]);
            }
            $url = [$scope => $url] + $currentParams;
            if (empty($url[$scope]['page'])) {
                unset($url[$scope]['page']);
            }
        }

        return $url;
    }
    public function hasPrev($model = null)
    {
        return $this->_hasPage($model, 'prev');
    }
    public function hasNext($model = null)
    {
        return $this->_hasPage($model, 'next');
    }
    public function hasPage($model = null, $page = 1)
    {
        if (is_numeric($model)) {
            $page = $model;
            $model = null;
        }
        $paging = $this->params($model);
        if ($paging === []) {
            return false;
        }

        return $page <= $paging['pageCount'];
    }
    protected function _hasPage($model, $page)
    {
        $params = $this->params($model);

        return !empty($params) && $params[$page . 'Page'];
    }
    public function defaultModel($model = null)
    {
        if ($model !== null) {
            $this->_defaultModel = $model;
        }
        if ($this->_defaultModel) {
            return $this->_defaultModel;
        }
        if (!$this->request->getParam('paging')) {
            return null;
        }
        list($this->_defaultModel) = array_keys($this->request->getParam('paging'));

        return $this->_defaultModel;
    }
    public function counter($options = [])
    {
        if (is_string($options)) {
            $options = ['format' => $options];
        }

        $options += [
            'model' => $this->defaultModel(),
            'format' => 'pages',
        ];

        $paging = $this->params($options['model']);
        if (!$paging['pageCount']) {
            $paging['pageCount'] = 1;
        }
        $start = 0;
        if ($paging['count'] >= 1) {
            $start = (($paging['page'] - 1) * $paging['perPage']) + 1;
        }
        $end = $start + $paging['perPage'] - 1;
        if ($paging['count'] < $end) {
            $end = $paging['count'];
        }

        switch ($options['format']) {
            case 'range':
            case 'pages':
                $template = 'counter' . ucfirst($options['format']);
                break;
            default:
                $template = 'counterCustom';
                $this->templater()->add([$template => $options['format']]);
        }
        $map = array_map([$this->Number, 'format'], [
            'page' => $paging['page'],
            'pages' => $paging['pageCount'],
            'current' => $paging['current'],
            'count' => $paging['count'],
            'start' => $start,
            'end' => $end
        ]);

        $map += [
            'model' => strtolower(Inflector::humanize(Inflector::tableize($options['model'])))
        ];

        return $this->templater()->format($template, $map);
    }
    public function numbers(array $options = [])
    {
        $defaults = [
            'before' => null, 'after' => null, 'model' => $this->defaultModel(),
            'modulus' => 8, 'first' => null, 'last' => null, 'url' => []
        ];
        $options += $defaults;

        $params = (array)$this->params($options['model']) + ['page' => 1];
        if ($params['pageCount'] <= 1) {
            return false;
        }

        $templater = $this->templater();
        if (isset($options['templates'])) {
            $templater->push();
            $method = is_string($options['templates']) ? 'load' : 'add';
            $templater->{$method}($options['templates']);
        }

        if ($options['modulus'] !== false && $params['pageCount'] > $options['modulus']) {
            $out = $this->_modulusNumbers($templater, $params, $options);
        } else {
            $out = $this->_numbers($templater, $params, $options);
        }

        if (isset($options['templates'])) {
            $templater->pop();
        }

        return $out;
    }
    protected function _getNumbersStartAndEnd($params, $options)
    {
        $half = (int)($options['modulus'] / 2);
        $end = max(1 + $options['modulus'], $params['page'] + $half);
        $start = min($params['pageCount'] - $options['modulus'], $params['page'] - $half - $options['modulus'] % 2);

        if ($options['first']) {
            $first = is_int($options['first']) ? $options['first'] : 1;

            if ($start <= $first + 2) {
                $start = 1;
            }
        }

        if ($options['last']) {
            $last = is_int($options['last']) ? $options['last'] : 1;

            if ($end >= $params['pageCount'] - $last - 1) {
                $end = $params['pageCount'];
            }
        }

        $end = min($params['pageCount'], $end);
        $start = max(1, $start);

        return [$start, $end];
    }
    protected function _formatNumber($templater, $options)
    {
        $url = array_merge($options['url'], ['page' => $options['page']]);
        $vars = [
            'text' => $options['text'],
            'url' => $this->generateUrl($url, $options['model']),
        ];

        return $templater->format('number', $vars);
    }
    protected function _modulusNumbers($templater, $params, $options)
    {
        $out = '';
        $ellipsis = $templater->format('ellipsis', []);

        list($start, $end) = $this->_getNumbersStartAndEnd($params, $options);

        $out .= $this->_firstNumber($ellipsis, $params, $start, $options);
        $out .= $options['before'];

        for ($i = $start; $i < $params['page']; $i++) {
            $out .= $this->_formatNumber($templater, [
                'text' => $this->Number->format($i),
                'page' => $i,
                'model' => $options['model'],
                'url' => $options['url'],
            ]);
        }

        $url = array_merge($options['url'], ['page' => $params['page']]);
        $out .= $templater->format('current', [
            'text' => $this->Number->format($params['page']),
            'url' => $this->generateUrl($url, $options['model']),
        ]);

        $start = $params['page'] + 1;
        $i = $start;
        while ($i < $end) {
            $out .= $this->_formatNumber($templater, [
                'text' => $this->Number->format($i),
                'page' => $i,
                'model' => $options['model'],
                'url' => $options['url'],
            ]);
            $i++;
        }

        if ($end != $params['page']) {
            $out .= $this->_formatNumber($templater, [
                'text' => $this->Number->format($i),
                'page' => $end,
                'model' => $options['model'],
                'url' => $options['url'],
            ]);
        }

        $out .= $options['after'];
        $out .= $this->_lastNumber($ellipsis, $params, $end, $options);

        return $out;
    }
    protected function _firstNumber($ellipsis, $params, $start, $options)
    {
        $out = '';
        $first = is_int($options['first']) ? $options['first'] : 0;
        if ($options['first'] && $start > 1) {
            $offset = ($start <= $first) ? $start - 1 : $options['first'];
            $out .= $this->first($offset, $options);
            if ($first < $start - 1) {
                $out .= $ellipsis;
            }
        }

        return $out;
    }
    protected function _lastNumber($ellipsis, $params, $end, $options)
    {
        $out = '';
        $last = is_int($options['last']) ? $options['last'] : 0;
        if ($options['last'] && $end < $params['pageCount']) {
            $offset = ($params['pageCount'] < $end + $last) ? $params['pageCount'] - $end : $options['last'];
            if ($offset <= $options['last'] && $params['pageCount'] - $end > $last) {
                $out .= $ellipsis;
            }
            $out .= $this->last($offset, $options);
        }

        return $out;
    }
    protected function _numbers($templater, $params, $options)
    {
        $out = '';
        $out .= $options['before'];
        for ($i = 1; $i <= $params['pageCount']; $i++) {
            $url = array_merge($options['url'], ['page' => $i]);
            if ($i == $params['page']) {
                $out .= $templater->format('current', [
                    'text' => $this->Number->format($params['page']),
                    'url' => $this->generateUrl($url, $options['model']),
                ]);
            } else {
                $vars = [
                    'text' => $this->Number->format($i),
                    'url' => $this->generateUrl($url, $options['model']),
                ];
                $out .= $templater->format('number', $vars);
            }
        }
        $out .= $options['after'];

        return $out;
    }
    public function first($first = '<< first', array $options = [])
    {
        $options += [
            'url' => [],
            'model' => $this->defaultModel(),
            'escape' => true
        ];

        $params = $this->params($options['model']);

        if ($params['pageCount'] <= 1) {
            return false;
        }

        $out = '';

        if (is_int($first) && $params['page'] >= $first) {
            for ($i = 1; $i <= $first; $i++) {
                $url = array_merge($options['url'], ['page' => $i]);
                $out .= $this->templater()->format('number', [
                    'url' => $this->generateUrl($url, $options['model']),
                    'text' => $this->Number->format($i)
                ]);
            }
        } elseif ($params['page'] > 1 && is_string($first)) {
            $first = $options['escape'] ? h($first) : $first;
            $out .= $this->templater()->format('first', [
                'url' => $this->generateUrl(['page' => 1], $options['model']),
                'text' => $first
            ]);
        }

        return $out;
    }
    public function last($last = 'last >>', array $options = [])
    {
        $options += [
            'model' => $this->defaultModel(),
            'escape' => true,
            'url' => []
        ];
        $params = $this->params($options['model']);

        if ($params['pageCount'] <= 1) {
            return false;
        }

        $out = '';
        $lower = (int)$params['pageCount'] - (int)$last + 1;

        if (is_int($last) && $params['page'] <= $lower) {
            for ($i = $lower; $i <= $params['pageCount']; $i++) {
                $url = array_merge($options['url'], ['page' => $i]);
                $out .= $this->templater()->format('number', [
                    'url' => $this->generateUrl($url, $options['model']),
                    'text' => $this->Number->format($i)
                ]);
            }
        } elseif ($params['page'] < $params['pageCount'] && is_string($last)) {
            $last = $options['escape'] ? h($last) : $last;
            $out .= $this->templater()->format('last', [
                'url' => $this->generateUrl(['page' => $params['pageCount']], $options['model']),
                'text' => $last
            ]);
        }

        return $out;
    }
    public function meta(array $options = [])
    {
        $options += [
                'model' => null,
                'block' => false,
                'prev' => true,
                'next' => true,
                'first' => false,
                'last' => false
            ];

        $model = isset($options['model']) ? $options['model'] : null;
        $params = $this->params($model);
        $links = [];

        if ($options['prev'] && $this->hasPrev()) {
            $links[] = $this->Html->meta('prev', $this->generateUrl(['page' => $params['page'] - 1], null, true));
        }

        if ($options['next'] && $this->hasNext()) {
            $links[] = $this->Html->meta('next', $this->generateUrl(['page' => $params['page'] + 1], null, true));
        }

        if ($options['first']) {
            $links[] = $this->Html->meta('first', $this->generateUrl(['page' => 1], null, true));
        }

        if ($options['last']) {
            $links[] = $this->Html->meta('last', $this->generateUrl(['page' => $params['pageCount']], null, true));
        }

        $out = implode($links);

        if ($options['block'] === true) {
            $options['block'] = __FUNCTION__;
        }

        if ($options['block']) {
            $this->_View->append($options['block'], $out);

            return null;
        }

        return $out;
    }
    public function implementedEvents()
    {
        return [];
    }
    public function limitControl(array $limits = [], $default = null, array $options = [])
    {
        $out = $this->Form->create(null, ['type' => 'get']);

        if (empty($default) || !is_numeric($default)) {
            $default = $this->param('perPage');
        }

        if (empty($limits)) {
            $limits = [
                '20' => '20',
                '50' => '50',
                '100' => '100'
            ];
        }

        $out .= $this->Form->control('limit', $options + [
                'type' => 'select',
                'label' => __('View'),
                'value' => $default,
                'options' => $limits,
                'onChange' => 'this.form.submit()'
            ]);
        $out .= $this->Form->end();

        return $out;
    }
}
