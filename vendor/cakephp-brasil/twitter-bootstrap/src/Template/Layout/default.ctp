<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta name="rating" content="General">
        <meta name="AUTHOR" content="https://www.facebook.com/profile.php?id=100012578408914">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <!– Código do Schema.org também para o Google+ –>
        <meta itemprop=”name” content=”<?php echo "". $title; ?>“>
        <meta itemprop=”description” content=”<?php echo "". $sitedesc; ?>“>
        <meta itemprop=”image” content=”<?php echo "". $siteimage; ?>“>

        <!– Código do Open Graph –>
        <meta property="fb:app_id" content="1708650332774678" />
        <meta property="og:type" content="website">
        <meta property="og:locale" content="pt_BR">
        <meta property="og:url" content="<?php echo "". $page; ?>">
        <meta property="og:title" content="<?php echo "". $title; ?>">
        <meta property="og:site_name" content="WM BORRACHAS E ACESSORIOS">
        <meta property="og:description" content="<?php echo "". $sitedesc; ?>">
        <meta property="og:image" content="<?php echo "". $siteimage; ?>">
        <meta property="og:image:type" content="image/jpeg">
        <meta property="og:image:width" content="400"> 
        <meta property="og:image:height" content="400">    
        <meta name="revisit-after" content="1 days">
        <title>
            <?php echo "". $title; ?>
        </title>
        <?= $this->Html->charset() ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <title><?= $this->fetch('title') ?></title>    
        <?= $this->Html->meta('icon') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->Html->css('animate.css') ?>
        <?= $this->Html->css('bootstrap-reboot.css') ?>
        <?= $this->Html->css('bootstrap-grid.css') ?>
        <?= $this->Html->css('bootstrap.min.css') ?>   
        <?= $this->Html->css('home.css') ?>    
        <?= $this->Html->css('animate.css') ?>
        <?= $this->Html->script('jquery-3.2.1.slim.min.js') ?>
        <?= $this->Html->script('popper.min.js') ?>
        <?= $this->Html->script('bootstrap.min.js') ?>
    </head>
    <body style="">
        <div class="body" >
        <header class="navbar navbar-expand-lg navbar-dark cor-menu" >
            <a class="navbar-brand" href="#">
                <?php echo $this->Html->image('wm.png', ['class'=>'menu-logo animated pulse']) ?>
            </a>
            <button style="" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <i class="material-icons">&#xE5D4;</i>
            </button>    


    <div class="collapse navbar-collapse" id="navbarColor01">
      
        <?php
                    $default_nav_bar_left = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'nav-bar-left.ctp';
                    if (file_exists($default_nav_bar_left)) {
                        ob_start();
                        include $default_nav_bar_left;
                        echo ob_get_clean();
                    }
                    else {
                        echo $this->element('nav-bar-left');
                    }
                ?>
                <?php
                    $default_nav_bar_right = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'nav-bar-right.ctp';
                    if (file_exists($default_nav_bar_right)) {
                        ob_start();
                        include $default_nav_bar_right;
                        echo ob_get_clean();
                    }
                    else{
                        echo $this->element('nav-bar-right');
                    }
                ?>
    </div>
  </header>        
        
        <section class="" style="padding-top: 5%; padding-bottom: 5%; background-color: #FFFFFF !important;">
            
            <div class="container">
                <?= $this->Flash->render() ?>
            </div>
            <?= $this->fetch('content') ?>
        </section>
        <footer>
            <div class="footer1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 ">
                        <H4>Suporte Técnico <i class="material-icons">&#xE0BE;</i></H4>
                        <form >
                            <div class="form-group">
                                    <input  class="form-control" type="text" name="assunto" placeholder="Qual o assunto?">
                            </div>
                            <div class="form-group">
                                
                                    <input class="form-control" type="text"name="email" placeholder="Informe um endereço de e-mail valido">
                                
                            </div>
                            <div class="form-group">                           
                                <textarea  class="form-control" name="menssagem" placeholder="Digite sua menssagem" rows="5" ></textarea>                             
                            </div>
                            <button type="submit" class="btn btn-dark" style="margin-bottom: 20px">Enviar</button>
                        </form>
                    </div>
                    <div class="col-md-7 text-justify">
                        <small>
                        <h6>
                            TIAGO DE OLIVEIRA
                        </h6>
                        <p>
                            <i>tcdeoliveira@outlook.com</i>
                        </p>
                        <p><SPAN><b>FIXO </b>(85) 3294-55412 <i>ou</i> </SPAN><SPAN><b>CELULAR / WHATSAPP </b>(85) 99767-5909</SPAN></p>
                        </small>
                    </div>
                    
                    </div>
                </div>            
            </div>        
            <div class="footer2">
                <div class="container text-center">
                    <a href="https://www.facebook.com/tcwebdev" target="blanck">
                    <?php echo $this->Html->image('BUZZDESIGN.png', ['class'=>'buzz-logo']) ?>
                    <br>
                    <i>Desenvolvido por </i><span style="color: #FFC107 !important">TcWebDev</span>
                    </a>
                </div>
            </div>
        </footer>
        </div>
    </body>
</html>
