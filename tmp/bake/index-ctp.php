<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->columnType($field), ['binary', 'text']);
    })
    ->take(7);
?>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group" id="list-tab" role="tablist" style="margin-bottom: 20px;">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home"><?= $pluralHumanName ?></a>
                <CakePHPBakeOpenTag= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['<?= $singularHumanName ?>']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) CakePHPBakeCloseTag>
                <?php
                    $done = [];
                    foreach ($associations as $type => $data):
                        foreach ($data as $alias => $details):
                            if (!empty($details['navLink']) && $details['controller'] !== $this->name && !in_array($details['controller'], $done)):
                ?>
                    <CakePHPBakeOpenTag= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['<?= $this->_pluralHumanName($alias) ?>']), ['controller' => '<?= $details['controller'] ?>', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) CakePHPBakeCloseTag>
                    <CakePHPBakeOpenTag= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo  {0}', ['<?= $this->_singularHumanName($alias) ?> <i class="material-icons">add_circle</i>']), ['controller' => '<?= $details['controller'] ?>', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) CakePHPBakeCloseTag>
                <?php
                    $done[] = $details['controller'];
                            endif;
                        endforeach;
                    endforeach;
                ?>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content" id="nav-tabContent">
                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table  table-sm table-hover table-striped table-responsive-sm border-cor-ibhj">
                            <thead class="bg-cor-ibhj">
                                <tr>
                                    <?php foreach ($fields as $field): ?>
                                        <th><CakePHPBakeOpenTag= $this->Paginator->sort('<?= $field ?>') CakePHPBakeCloseTag></th>
                                    <?php endforeach; ?>
                                    <th class="actions"><CakePHPBakeOpenTag= __('Ações') CakePHPBakeCloseTag></th>
                                </tr>
                            </thead>
                            <tbody>
                                <CakePHPBakeOpenTagphp 
                                    foreach ($<?= $pluralVar ?> as $<?= $singularVar ?>): CakePHPBakeCloseTag>
                                        <tr>
                                            <?php        
                                                foreach ($fields as $field) {
                                                    $isKey = false;
                                                        if (!empty($associations['BelongsTo'])) {
                                                            foreach ($associations['BelongsTo'] as $alias => $details) {
                                                                if ($field === $details['foreignKey']) {
                                                                    $isKey = true;
                                            ?>
                                            <td>
                                                <CakePHPBakeOpenTag= $<?= $singularVar ?>->has('<?= $details['property'] ?>') ? $this->Html->link($<?= $singularVar ?>-><?= $details['property'] ?>-><?= $details['displayField'] ?>, ['controller' => '<?= $details['controller'] ?>', 'action' => 'view', $<?= $singularVar ?>-><?= $details['property'] ?>-><?= $details['primaryKey'][0] ?>]) : '' CakePHPBakeCloseTag>
                                            </td>
                                            <?php
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        if ($isKey !== true) {
                                                        if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
                                            ?>
                                            <td>
                                                <CakePHPBakeOpenTag= h($<?= $singularVar ?>-><?= $field ?>) CakePHPBakeCloseTag></td>
                                                <?php
                                                } else {
                                                ?>
                                                    <td><CakePHPBakeOpenTag= $this->Number->format($<?= $singularVar ?>-><?= $field ?>) CakePHPBakeCloseTag></td>
                                                <?php
                                                }
                                                }
                                            }
                                            $pk = '$' . $singularVar . '->' . $primaryKey[0];
                                            ?>
                                            <td class="actions" style="white-space:nowrap">
                                                <CakePHPBakeOpenTag= $this->Html->link(__('<i class="material-icons  btn-pequeno">&#xE5C6;</i>'), ['action' => 'view', <?= $pk ?>], ['class'=>'btn btn btn-outline-secondary btn-sm btn-pequeno', 'escape'=>false, 'title'=>'Exibir']) CakePHPBakeCloseTag>
                                                <CakePHPBakeOpenTag= $this->Html->link(__('<i class="material-icons  btn-pequeno">&#xE644;</i>'), ['action' => 'edit', <?= $pk ?>], ['class'=>'btn btn btn-outline-secondary btn-sm  btn-pequeno', 'escape'=>false, 'title'=>'Editar']) CakePHPBakeCloseTag>
                                                <CakePHPBakeOpenTag= $this->Form->postLink(__('<i class="material-icons  btn-pequeno">&#xE92B;</i>'), ['action' => 'delete', <?= $pk ?>], ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', <?= $pk ?>), 'class'=>'btn btn-danger btn-sm  btn-pequeno', 'escape'=>false, 'title'=>'Excluir']) CakePHPBakeCloseTag>
                                            </td>
                                        </tr>
                                <CakePHPBakeOpenTagphp 
                                    endforeach; 
                                CakePHPBakeCloseTag>
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <CakePHPBakeOpenTag= $this->Paginator->prev('&laquo; ' . __(''), ['escape'=>false]) CakePHPBakeCloseTag>
                                <CakePHPBakeOpenTag= $this->Paginator->numbers(['escape'=>false]) CakePHPBakeCloseTag>
                                <CakePHPBakeOpenTag= $this->Paginator->next(__('') . ' &raquo;', ['escape'=>false]) CakePHPBakeCloseTag>

                            </ul>
                            <p  class="pagination justify-content-end pagination-text"><CakePHPBakeOpenTag= $this->Paginator->counter(__('Página {{page}} de {{pages}}, exibindo {{current}} registros de
                            {{count}} encontrados.')) CakePHPBakeCloseTag></p>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
