<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PalavrasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PalavrasTable Test Case
 */
class PalavrasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PalavrasTable
     */
    public $Palavras;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.palavras',
        'app.congregados'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Palavras') ? [] : ['className' => PalavrasTable::class];
        $this->Palavras = TableRegistry::get('Palavras', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Palavras);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
