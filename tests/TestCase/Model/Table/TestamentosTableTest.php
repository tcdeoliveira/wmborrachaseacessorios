<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TestamentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TestamentosTable Test Case
 */
class TestamentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TestamentosTable
     */
    public $Testamentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.testamentos',
        'app.livros',
        'app.versiculos',
        'app.versions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Testamentos') ? [] : ['className' => TestamentosTable::class];
        $this->Testamentos = TableRegistry::get('Testamentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Testamentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
