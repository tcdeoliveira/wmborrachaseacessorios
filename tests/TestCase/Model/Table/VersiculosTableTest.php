<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VersiculosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VersiculosTable Test Case
 */
class VersiculosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VersiculosTable
     */
    public $Versiculos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.versiculos',
        'app.versions',
        'app.livros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Versiculos') ? [] : ['className' => VersiculosTable::class];
        $this->Versiculos = TableRegistry::get('Versiculos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Versiculos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
