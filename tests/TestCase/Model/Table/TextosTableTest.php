<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TextosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TextosTable Test Case
 */
class TextosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TextosTable
     */
    public $Textos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.textos',
        'app.autor_textos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Textos') ? [] : ['className' => TextosTable::class];
        $this->Textos = TableRegistry::get('Textos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Textos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
