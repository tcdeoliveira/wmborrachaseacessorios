<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CongregadosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CongregadosTable Test Case
 */
class CongregadosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CongregadosTable
     */
    public $Congregados;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.congregados'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Congregados') ? [] : ['className' => CongregadosTable::class];
        $this->Congregados = TableRegistry::get('Congregados', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Congregados);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
