<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;
class ProdutosController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.
        $this->Auth->allow(['index', 'view', 'search', 'categorias']);
    }
    public function index()
    {
        $this->paginate = [
            'contain' => ['ProdutoCategorias'],
            'limit'=>6
        ];
        $produtos = $this->paginate($this->Produtos);
        $this->loadmodel('ProdutoCategorias');
        $query = $this->ProdutoCategorias->find();
        $categorias = $query->all();
        $this->set(compact('produtos', 'categorias'));
    }
    public function categorias($id = null, $nome = null){
    $produtos = $this->Produtos->find('all',
                            [
                                    'conditions'=>[
                                        'produto_categoria_id'=>$id
                                    ]
                            ]
                    );
                    $this->paginate = [
                        
                        'conditions'=>[
                                        'produto_categoria_id'=>$id
                                    ]
                    ];
                    $produtos = $this->paginate($this->Produtos);
                    $this->loadmodel('ProdutoCategorias');
                    $query = $this->ProdutoCategorias->find();
                    $categorias = $query->all();
                    $this->set(compact('categorias'));
                    $this->set(compact('produtos'));
                    $this->set('_serialize', ['produtos']);
                   $this->render('index');    
    }
    public function search()
        {        
            if ($this->request->is('post')) {
                    $search = null;
                    if (isset($this->request->data['search'])) {
                            $search = $this->request->data['search'];
                    }
                    $produtos = $this->Produtos->find('all',
                            [
                                    'conditions'=>[
                                        'OR'=>[    'Produtos.nome LIKE'=>'%'.$search.'%' ]
                                    ]
                            ]
                    );
                    $this->paginate = [
                        
                        'conditions'=>[
                                       'OR'=>[    'Produtos.nome LIKE'=>'%'.$search.'%'  ],
                                    ]
                    ];
                    $produtos = $this->paginate($this->Produtos);
                    $this->loadmodel('ProdutoCategorias');
                    $query = $this->ProdutoCategorias->find();
                    $categorias = $query->all();
                    $this->set(compact('categorias'));
                    $this->set(compact('produtos'));
                    $this->set('_serialize', ['produtos']);
                   $this->render('index');
            } else {
                    throw new NotFoundException("Pagina nao encontrada");
            }            
        }
    public function view($url = null)
    {
        $query = $this->Produtos->find('all', [
            'contain' => ['ProdutoCategorias'],
            'conditions'=>[
                'url'=>$url
            ]
        ]);
        $produto = $query->first();

        $this->set('produto', $produto);
    }
}
