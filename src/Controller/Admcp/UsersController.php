<?php
namespace App\Controller\Admcp;
use Cake\Event\Event;
use App\Controller\AppController;
class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.
        //$this->Auth->allow(['index', 'view', 'search', 'edit']);
    }
    public function login(){     
        
        if($this->request->is('post'))/*Verifica se a requisição é uma requisição post*/{
            $user = $this->Auth->identify()/* Pega os dados enviados via post e verifica se existe um usuário com esses dados, se sim e a senha bater ele retorna os dados do usuário. */;
            debug($user);
            if($user){ // Te
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());            
            }
            unset($this->request->data['password']);
            
            $this->Flash->error('Usuário ou senha incorretos.');
        }
    }    
    public function logout(){
        $this->Auth->logout();
        return $this->redirect($this->referer());
    }
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
