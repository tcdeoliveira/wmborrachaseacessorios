<?php
namespace App\Controller\Admcp;

use App\Controller\AppController;
use Cake\Event\Event;
class ProdutoCategoriasController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        //$this->Auth->allow('login');
    }
    public function index()
    {
        $produtoCategorias = $this->paginate($this->ProdutoCategorias);

        $this->set(compact('produtoCategorias'));
    }
    public function view($id = null)
    {
        $produtoCategoria = $this->ProdutoCategorias->get($id, [
            'contain' => ['Produtos']
        ]);

        $this->set('produtoCategoria', $produtoCategoria);
    }
    public function add()
    {
        $produtoCategoria = $this->ProdutoCategorias->newEntity();
        if ($this->request->is('post')) {
            $produtoCategoria = $this->ProdutoCategorias->patchEntity($produtoCategoria, $this->request->getData());
            if ($this->ProdutoCategorias->save($produtoCategoria)) {
                $this->Flash->success(__('The produto categoria has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The produto categoria could not be saved. Please, try again.'));
        }
        $this->set(compact('produtoCategoria'));
    }
    public function edit($id = null)
    {
        $produtoCategoria = $this->ProdutoCategorias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $produtoCategoria = $this->ProdutoCategorias->patchEntity($produtoCategoria, $this->request->getData());
            if ($this->ProdutoCategorias->save($produtoCategoria)) {
                $this->Flash->success(__('The produto categoria has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The produto categoria could not be saved. Please, try again.'));
        }
        $this->set(compact('produtoCategoria'));
    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $produtoCategoria = $this->ProdutoCategorias->get($id);
        $this->loadmodel('Produtos');
        $test = $this->Produtos->find()->where(['produto_categoria_id'=>$id])->all();
        $cont = $test->count();
        if($cont>0):
            $this->Flash->error(__('Não foi possivel deletar esta categoria porque ela ainda existem produtos vinculados a ela.'));
            else:
                if ($this->ProdutoCategorias->delete($produtoCategoria)) {
                    $this->Flash->success(__('Categoria deletada com sucesso.'));
                } else {
                    $this->Flash->error(__('Não foi possivel deletar esta categoria.'));
                }  
        endif;        
        return $this->redirect(['action' => 'index']);
    }
}
