<?php
namespace App\Controller\Admcp;
use App\Controller\AppController;
use Cake\Event\Event;
class ProdutosController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        //$this->Auth->allow('login');
    }
    public function index()
    {
        $this->paginate = [
            'contain' => ['ProdutoCategorias'],
            'limit'=>2
        ];
        $produtos = $this->paginate($this->Produtos);        
         $data =1;
        $this->set(compact('produtos', 'data'));
        

// Make $color, $type, and $base_price
// available to the view:

$this->set($data);
    }
    public function add()
    {
        $produto = $this->Produtos->newEntity();
        if ($this->request->is('post')) {
            $produto = $this->Produtos->patchEntity($produto, $this->request->getData());  
            $imagem =$this->request->getData('imagem');          
            $url = $this->acentos($produto->nome);
            debug($url);
            $tipo = $this->pegarExtencao($imagem['name']);
            if ($this->validarArquivo($tipo)): //inicio IF VALIDAR IMAGEM
                $nome = $this->send($imagem['tmp_name'], "img/produtos/", $tipo);
                $produto->foto = $nome; 
                $produto->destaque = false;     
                $url = $this->acentos($produto->nome); 
                $produto->url =   str_replace(" ", "-", $url);       
                if ($this->Produtos->save($produto)) {
                    $this->Flash->success(__('The texto has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }        
            endif; 
            $this->Flash->error(__('The texto could not be saved. Please, try again.'));
            
        }
        $produtoCategorias = $this->Produtos->ProdutoCategorias->find('list', ['limit' => 200]);
        $this->set(compact('produto', 'produtoCategorias'));
    } 
    public function favoritar($id = null)
    {
        $produto = $this->Produtos->get($id);
            if($produto->destaque):
            $produto->destaque = false;
            $this->Produtos->save($produto);
            $this->Flash->success(__('Produto removido da lista de destaques com sucesso.'));
            return $this->redirect($this->referer());
            else:
                $test = $this->Produtos->find()->where(['destaque'])->all();
                $re = $test->count();
                if($re < 4):
                    $produto->destaque = true;
                    $this->Produtos->save($produto);
                    $this->Flash->success(__('Produto adcionado a lista de destaques com sucesso.'));
                    return $this->redirect($this->referer());
                else:
                    $this->Flash->error(__('Você já atingiu o limite máximo de produtos destacados. Remova um produto da lista para adcionar outro.'));
                    return $this->redirect($this->referer());
                endif;                
            endif;                       
    }  
    public function edit($id = null)
    {
        $produto = $this->Produtos->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $produto = $this->Produtos->patchEntity($produto, $this->request->getData());
            debug($this->request->getData());
            $imagem = $this->request->getData('imagem');
            if($imagem['tmp_name']):
                $tipo = $this->pegarExtencao($imagem['name']);
                if ($this->validarArquivo($tipo)): //inicio IF VALIDAR IMAGEM
                    $this->deletarfoto($produto->foto);
                    $nome = $this->send($imagem['tmp_name'], "img/produtos/", $tipo);
                    $produto->foto = $nome;   

                    else:
                       $produtoCategorias = $this->Produtos->ProdutoCategorias->find('list', ['limit' => 200]);
                       $this->set(compact('produto', 'produtoCategorias')); 
                       return $this->Flash->error(__("<b>$tipo</b> Não é um tipo de Arquivo não suportado.<br>Use arquivos do tipo <b>JPG, JPEG ou PNG</b>."));
                endif;
            endif;
            $url = str_replace(" ", "-", $produto->nome);
            $produto->url = $url;
            if ($this->Produtos->save($produto)) { 
                $this->Flash->success(__("<b>$produto->nome </b> foi editado com sucesso."));
                return $this->redirect(['action' => 'index']);
            }
        }
        $produtoCategorias = $this->Produtos->ProdutoCategorias->find('list', ['limit' => 200]);
        $this->set(compact('produto', 'produtoCategorias'));
    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $produto = $this->Produtos->get($id);
        if ($this->Produtos->delete($produto)) {
            $this->Flash->success(__('The produto has been deleted.'));
        } else {
            $this->Flash->error(__('The produto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
