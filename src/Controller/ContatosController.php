<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;
class ContatosController extends AppController
{
    
    use MailerAwareTrait;
    public function enviar(){
        if ($this->request->is('post')) {
            $email = $this->request->getData();      
            if($this->getMailer('Ibhj')->send('contato', [$email])):
                $this->Flash->success(__('Sua mensagem foi enviada. Em breve enviaremos uma resposta.'));
                return $this->redirect('/');
            endif;
            ; 
        }
    }
}
