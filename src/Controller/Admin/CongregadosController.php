<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Congregados Controller
 *
 * @property \App\Model\Table\CongregadosTable $Congregados
 *
 * @method \App\Model\Entity\Congregado[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CongregadosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $congregados = $this->paginate($this->Congregados);

        $this->set(compact('congregados'));
    }

    /**
     * View method
     *
     * @param string|null $id Congregado id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $congregado = $this->Congregados->get($id, [
            'contain' => []
        ]);

        $this->set('congregado', $congregado);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $congregado = $this->Congregados->newEntity();
        if ($this->request->is('post')) {
            $congregado = $this->Congregados->patchEntity($congregado, $this->request->getData());
            if ($this->Congregados->save($congregado)) {
                $this->Flash->success(__('The congregado has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The congregado could not be saved. Please, try again.'));
        }
        $this->set(compact('congregado'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Congregado id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $congregado = $this->Congregados->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $congregado = $this->Congregados->patchEntity($congregado, $this->request->getData());
            if ($this->Congregados->save($congregado)) {
                $this->Flash->success(__('The congregado has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The congregado could not be saved. Please, try again.'));
        }
        $this->set(compact('congregado'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Congregado id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $congregado = $this->Congregados->get($id);
        if ($this->Congregados->delete($congregado)) {
            $this->Flash->success(__('The congregado has been deleted.'));
        } else {
            $this->Flash->error(__('The congregado could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
