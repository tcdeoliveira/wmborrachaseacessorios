<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Palavras Controller
 *
 * @property \App\Model\Table\PalavrasTable $Palavras
 *
 * @method \App\Model\Entity\Palavra[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PalavrasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Congregados']
        ];
        $palavras = $this->paginate($this->Palavras);

        $this->set(compact('palavras'));
    }

    /**
     * View method
     *
     * @param string|null $id Palavra id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $palavra = $this->Palavras->get($id, [
            'contain' => ['Congregados']
        ]);

        $this->set('palavra', $palavra);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $palavra = $this->Palavras->newEntity();
        if ($this->request->is('post')) {
            $palavra = $this->Palavras->patchEntity($palavra, $this->request->getData());
            if ($this->Palavras->save($palavra)) {
                $this->Flash->success(__('The palavra has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The palavra could not be saved. Please, try again.'));
        }
        $congregados = $this->Palavras->Congregados->find('list', ['limit' => 200]);
        $this->set(compact('palavra', 'congregados'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Palavra id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $palavra = $this->Palavras->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $palavra = $this->Palavras->patchEntity($palavra, $this->request->getData());
            if ($this->Palavras->save($palavra)) {
                $this->Flash->success(__('The palavra has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The palavra could not be saved. Please, try again.'));
        }
        $congregados = $this->Palavras->Congregados->find('list', ['limit' => 200]);
        $this->set(compact('palavra', 'congregados'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Palavra id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $palavra = $this->Palavras->get($id);
        if ($this->Palavras->delete($palavra)) {
            $this->Flash->success(__('The palavra has been deleted.'));
        } else {
            $this->Flash->error(__('The palavra could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
