<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Textos Controller
 *
 * @property \App\Model\Table\TextosTable $Textos
 *
 * @method \App\Model\Entity\Texto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TextosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['AutorTextos']
        ];
        $textos = $this->paginate($this->Textos);

        $this->set(compact('textos'));
    }

    /**
     * View method
     *
     * @param string|null $id Texto id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $texto = $this->Textos->get($id, [
            'contain' => ['AutorTextos']
        ]);

        $this->set('texto', $texto);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $texto = $this->Textos->newEntity();
        if ($this->request->is('post')) {
            $texto = $this->Textos->patchEntity($texto, $this->request->getData());
            if ($this->Textos->save($texto)) {
                $this->Flash->success(__('The texto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The texto could not be saved. Please, try again.'));
        }
        $autorTextos = $this->Textos->AutorTextos->find('list', ['limit' => 200]);
        $this->set(compact('texto', 'autorTextos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Texto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $texto = $this->Textos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $texto = $this->Textos->patchEntity($texto, $this->request->getData());
            if ($this->Textos->save($texto)) {
                $this->Flash->success(__('The texto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The texto could not be saved. Please, try again.'));
        }
        $autorTextos = $this->Textos->AutorTextos->find('list', ['limit' => 200]);
        $this->set(compact('texto', 'autorTextos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Texto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $texto = $this->Textos->get($id);
        if ($this->Textos->delete($texto)) {
            $this->Flash->success(__('The texto has been deleted.'));
        } else {
            $this->Flash->error(__('The texto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
