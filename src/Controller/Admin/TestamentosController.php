<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Testamentos Controller
 *
 * @property \App\Model\Table\TestamentosTable $Testamentos
 *
 * @method \App\Model\Entity\Testamento[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TestamentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $testamentos = $this->paginate($this->Testamentos);

        $this->set(compact('testamentos'));
    }

    /**
     * View method
     *
     * @param string|null $id Testamento id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $testamento = $this->Testamentos->get($id, [
            'contain' => ['Livros']
        ]);

        $this->set('testamento', $testamento);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $testamento = $this->Testamentos->newEntity();
        if ($this->request->is('post')) {
            $testamento = $this->Testamentos->patchEntity($testamento, $this->request->getData());
            if ($this->Testamentos->save($testamento)) {
                $this->Flash->success(__('The testamento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The testamento could not be saved. Please, try again.'));
        }
        $this->set(compact('testamento'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Testamento id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $testamento = $this->Testamentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $testamento = $this->Testamentos->patchEntity($testamento, $this->request->getData());
            if ($this->Testamentos->save($testamento)) {
                $this->Flash->success(__('The testamento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The testamento could not be saved. Please, try again.'));
        }
        $this->set(compact('testamento'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Testamento id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $testamento = $this->Testamentos->get($id);
        if ($this->Testamentos->delete($testamento)) {
            $this->Flash->success(__('The testamento has been deleted.'));
        } else {
            $this->Flash->error(__('The testamento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
