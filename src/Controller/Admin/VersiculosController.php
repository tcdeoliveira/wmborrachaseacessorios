<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Versiculos Controller
 *
 * @property \App\Model\Table\VersiculosTable $Versiculos
 *
 * @method \App\Model\Entity\Versiculo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VersiculosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Versions', 'Livros']
        ];
        $versiculos = $this->paginate($this->Versiculos);

        $this->set(compact('versiculos'));
    }

    /**
     * View method
     *
     * @param string|null $id Versiculo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $versiculo = $this->Versiculos->get($id, [
            'contain' => ['Versions', 'Livros']
        ]);

        $this->set('versiculo', $versiculo);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $versiculo = $this->Versiculos->newEntity();
        if ($this->request->is('post')) {
            $versiculo = $this->Versiculos->patchEntity($versiculo, $this->request->getData());
            if ($this->Versiculos->save($versiculo)) {
                $this->Flash->success(__('The versiculo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The versiculo could not be saved. Please, try again.'));
        }
        $versions = $this->Versiculos->Versions->find('list', ['limit' => 200]);
        $livros = $this->Versiculos->Livros->find('list', ['limit' => 200]);
        $this->set(compact('versiculo', 'versions', 'livros'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Versiculo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $versiculo = $this->Versiculos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $versiculo = $this->Versiculos->patchEntity($versiculo, $this->request->getData());
            if ($this->Versiculos->save($versiculo)) {
                $this->Flash->success(__('The versiculo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The versiculo could not be saved. Please, try again.'));
        }
        $versions = $this->Versiculos->Versions->find('list', ['limit' => 200]);
        $livros = $this->Versiculos->Livros->find('list', ['limit' => 200]);
        $this->set(compact('versiculo', 'versions', 'livros'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Versiculo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $versiculo = $this->Versiculos->get($id);
        if ($this->Versiculos->delete($versiculo)) {
            $this->Flash->success(__('The versiculo has been deleted.'));
        } else {
            $this->Flash->error(__('The versiculo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
