<?php
namespace App\Controller;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Utility\Text;
class AppController extends Controller
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authError'    => 'Você não está autorizado a acessar este conteúdo!',
            'authorize' => ['Controller'], // Adicione está linha            
            'authenticate'=>[
                'Form'=>[
                    'fields'=>['username'=>'email']
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
                'prefix'=>'admcp'
            ]
        ]);
        $this->Auth->allow(['login', 'logout', 'display']);
    }
    public function isAuthorized($user)
    {
        if (isset($user)) {
            return true;
        }
        return false;
    }
    
    
    public function beforeRender(Event $event)
    {   
        $this->loadComponent('Auth');
        $this->set('user_auth', $this->Auth->user()); 
        $prefix = isset($this->request->params['prefix']) ? $this->request->params['prefix'] : null;
       	 if ($prefix == 'admcp') {
            	$this->viewBuilder()->theme('TwitterBootstrap');
       	 }
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }        
    }
    public function acentos($str) {
        // assume $str esteja em UTF-8
        $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
        $to = "aaaaeeiooouucAAAAEEIOOOUUC";        
        $keys = array();
        $values = array();
        preg_match_all('/./u', $from, $keys);
        preg_match_all('/./u', $to, $values);
        $mapping = array_combine($keys[0], $values[0]);
        return strtr($str, $mapping);
    }
    public function validarArquivo($ext){
        $allowed = ['jpeg', 'jpg', 'png', 'PNG', 'JPG', 'JPEG'];
        return in_array( $ext, $allowed);        
    }
    public function pegarExtencao($filename){
        $ext = substr($filename, -4); 
        if($ext[0] == '.'){
              $ext = substr($filename, -3);
        }  
        
        return $ext;
    }
    public function deletarfoto( $nome  )
    {        
        if (file_exists(WWW_ROOT.'img/produtos/'.$nome)):
            $file = new File(WWW_ROOT.'img/produtos/'.$nome);
            $file->delete();
            return 0;
        endif;    
        return false;
    }
    public function send( $tempname, $pasta, $tipo  )
    {
        $max_files = 1;   
        $file_tmp_name = $tempname;
        $dir = WWW_ROOT.$pasta;
        $filename = Text::uuid().".".$tipo;
        if( is_uploaded_file( $file_tmp_name ) ){
            move_uploaded_file($file_tmp_name, $dir.DS.$filename);
            return $filename;
        }                                  
    }
    public function upload()
    {
        if ( !empty( $this->request->data ) ) {
           $name = $this->Upload->send($this->request->data['uploadfile']);
        }
    }  
}
