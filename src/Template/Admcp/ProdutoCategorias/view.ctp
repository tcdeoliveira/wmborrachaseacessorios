<div class="container">
    <div class="row">
    
    <div class="col-md-9">
        <div class="produtoCategorias col-lg-10 col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-right"><?= h($produtoCategoria->id) ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover">
                                                        <tr>
                        <th>Nome</th>
                        <td><?= h($produtoCategoria->nome) ?></td>
                    </tr>
                                                                                <tr>
                        <th>Id</th>
                        <td><?= $this->Number->format($produtoCategoria->id) ?></td>
                    </tr>
                                                                </table>
                                        <div class="related">
                    <?php if (!empty($produtoCategoria->produtos)): ?>
                    <h4><?= __('Related {0}', ['Produtos']) ?></h4>
                    <table class="table table-striped table-hover">
                        <tr>
                                        <th>Id</th>
                                        <th>Nome</th>
                                        <th>Descrisao</th>
                                        <th>Foto</th>
                                        <th>Produto Categoria Id</th>
                                        <th>Destaque</th>
                                        <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($produtoCategoria->produtos as $produtos): ?>
                        <tr>
                            <td><?= h($produtos->id) ?></td>
                            <td><?= h($produtos->nome) ?></td>
                            <td><?= h($produtos->descrisao) ?></td>
                            <td><?= h($produtos->foto) ?></td>
                            <td><?= h($produtos->produto_categoria_id) ?></td>
                            <td><?= h($produtos->destaque) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Produtos', 'action' => 'view', $produtos->id]) ?>

                                <?= $this->Html->link(__('Edit'), ['controller' => 'Produtos', 'action' => 'edit', $produtos->id]) ?>

                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Produtos', 'action' => 'delete', $produtos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $produtos->id)]) ?>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php endif; ?>
                </div>
                </div>
    </div>
    </div>
</div>
        <div class="col-md-3">
        <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
            <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Editar {0}', ['Produto Categoria']), ['action' => 'edit', $produtoCategoria->id], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?> 
            <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $produtoCategoria->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $produtoCategoria->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
            ?>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Produto Categoria']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
            <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Produto Categorias ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Produtos']), ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Produto']), ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    </div>
    </div>
        </div>
</div>
