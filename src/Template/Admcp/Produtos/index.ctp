<?php $this->set("title","WM | Produtos");  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","www.wmborrachaseacessorios.com.br");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<style type="text/css">
    .nav-produtos{
        color: #FFFFFF !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group" id="list-tab" role="tablist" style="margin-bottom: 20px;">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Produtos</a>
                <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Produto']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                    <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Categorias']), ['controller' => 'ProdutoCategorias', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Nova  {0}', ['Categoria']), ['controller' => 'ProdutoCategorias', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content" id="nav-tabContent">
                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table  table-sm table-hover table-striped table-responsive-sm border-cor-ibhj">
                            <thead class="bg-cor-ibhj">
                                <tr>
                                    <th><?= $this->Paginator->sort('id') ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('url') ?></th>
                                    <th><?= $this->Paginator->sort('produto_categoria_id') ?></th>
                                    <th><?= $this->Paginator->sort('destaque') ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($produtos as $produto): ?>
                                        <tr>
                                            <td>
                                                <?= $this->Number->format($produto->id) ?>
                                                    
                                            </td>
                                            <td>
                                                <?= h($produto->nome) ?>
                                                    
                                            </td>
                                            <td>
                                                <?= h($produto->url) ?>
                                                    
                                            </td>
                                            <td>
                                                <?= $produto->produto_categoria->nome ?>
                                            </td>
                                            <td>
                                                <?php
                                                    if($produto->destaque):
                                                    ?>
                                                        <a class="favoritar-produto-favorito" href="<?php echo $this->Url->build('/admcp/produtos/favoritar/'.$produto->id) ?>"><i class="material-icons">star_rate</i></a>
                                                        <?php
                                                        else:
                                                        ?>
                                                        <a class="favoritar-produto" href="<?php echo $this->Url->build('/admcp/produtos/favoritar/'.$produto->id) ?>"><i class="material-icons">star_rate</i></a>
                                                        <?php
                                                    endif;
                                                ?>
                                            </td>
                                            <td class="actions" style="white-space:nowrap">
                                                
                                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $produto->id], ['class'=>'btn btn-dark btn-sm', 'escape'=>false, 'title'=>'Editar']) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $produto->id], ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $produto->id), 'class'=>'btn btn-danger btn-sm', 'escape'=>false, 'title'=>'Excluir']) ?>
                                            </td>
                                        </tr>
                                <?php 
                                    endforeach; 
                                ?>
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <?= $this->Paginator->prev('&laquo; ' . __(''), ['escape'=>false]) ?>
                                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                                <?= $this->Paginator->next(__('') . ' &raquo;', ['escape'=>false]) ?>

                            </ul>
                            <p  class="pagination justify-content-end pagination-text"><?= $this->Paginator->counter(__('Página {{page}} de {{pages}}, exibindo {{current}} registros de
                            {{count}} encontrados.')) ?></p>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
<?php echo $data ?>