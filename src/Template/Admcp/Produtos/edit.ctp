<?php $this->set("title","WM | $produto->nome");  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","www.wmborrachaseacessorios.com.br");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<script>
$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
});
</script>
<style type="text/css">
    .nav-produtos{
        color: #FFFFFF !important;
    }
</style>
<?= $this->Form->create($produto, ['type'=>'file']) ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card text-white bg-warning" >
              <img class="card-img-top" src="<?php echo $this->Url->build("/img/produtos/$produto->foto"); ?>" alt="<?php echo $produto->nome ?>">
              <div class="card-body">                
                 <div class="form-group">
                        <div class="input-group mb-3">

  <div class="input-group-prepend">

    <span class='btn input-group-addon btn-dark' style="cursor: pointer !important" href='javascript:;'>
                                <i class="material-icons">&#xE439;</i>
                                <input type="file"  style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="imagem"   onchange='$("#upload-file-info").html($(this).val());'>
                                </span>
  </div>
   <input type="text" class="form-control" style="border-color: #495057 !important" placeholder="Alterar imagem"  readonly>
</div>
                    </div>
              </div>
            </div>
        </div>        
        <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= "Editar $produto->nome" ?></h3>
            </div>
            <div class="box-body">
                
                <fieldset>
                    <?php
                                    echo $this->Form->input('nome');
                                    echo $this->Form->input('descrisao');
                                    echo $this->Form->input('produto_categoria_id', ['options' => $produtoCategorias]);
                                ?>
                </fieldset>
                <button type="submit" class="btn btn-warning" style="margin-bottom: 20px;">Salvar</button>
                <?= $this->Form->end() ?>
            </div>
        </div>
        </div>
        <div class="col-md-3">

            <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
                                    <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $produto->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $produto->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
                    ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Produtos ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', 'Categorias'), ['controller' => 'ProdutoCategorias', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>

                <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Nova {0}', 'Categoria'), ['controller' => 'ProdutoCategorias', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>

                            </div>
        </div>
    </div>
</div>    
