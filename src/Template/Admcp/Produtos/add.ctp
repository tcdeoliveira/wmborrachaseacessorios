<?php $this->set("title","WM | Novo Produto");  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","www.wmborrachaseacessorios.com.br");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<script>
$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
});
</script>  
<style type="text/css">
    .nav-produtos{
        color: #FFFFFF !important;
    }
</style>
<div class="container">
    <div class="row">
        
        <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= 'Cadastrar Produto' ?></h3>
            </div>
            <div class="box-body">
                <?php echo $this->Form->create($produto, ['type' => 'file']); ?>
                <fieldset>
                    <?php
                                    echo $this->Form->input('nome');
                                    echo $this->Form->input('descrisao', ['type'=>'textarea', 'rows'=>5, 'label'=>'Descrição']);
                    ?>
                    <div class="form-group">
                        <label for="img">Imagem</label>
                        <div class="input-group mb-3">

  <div class="input-group-prepend">

    <span class='btn input-group-addon btn-warning' style="cursor: pointer !important" href='javascript:;'>
                                <i class="material-icons">&#xE439;</i>
                                <input type="file"  style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="imagem"   onchange='$("#upload-file-info").html($(this).val());'>
                                </span>
  </div>
   <input type="text" class="form-control" placeholder="Alterar imagem"  readonly>
</div>
                    </div>
  
   

                    
                    <?php
                                    echo $this->Form->input('produto_categoria_id', ['options' => $produtoCategorias]);
                                ?>
                </fieldset>
                <button type="submit" class="btn btn-warning" style="margin-bottom: 20px;">Salvar</button>
                <?= $this->Form->end() ?>
            </div>
        </div>
        </div>
        <div class="col-md-3">
            <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Produtos ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', 'Produto Categorias'), ['controller' => 'ProdutoCategorias', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>

                <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', 'Produto Categoria'), ['controller' => 'ProdutoCategorias', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>

                            </div>
        </div>
    </div>
</div>    




