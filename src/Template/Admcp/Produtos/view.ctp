<div class="container">
    <div class="row">
    
    <div class="col-md-9">
        <div class="produtos col-lg-10 col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-right"><?= h($produto->id) ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover">
                                                        <tr>
                        <th>Nome</th>
                        <td><?= h($produto->nome) ?></td>
                    </tr>
                                                        <tr>
                        <th>Descrisao</th>
                        <td><?= h($produto->descrisao) ?></td>
                    </tr>
                                                        <tr>
                        <th>Foto</th>
                        <td><?= h($produto->foto) ?></td>
                    </tr>
                                                        <tr>
                        <th>Produto Categoria</th>
                        <td><?= $produto->has('produto_categoria') ? $this->Html->link($produto->produto_categoria->id, ['controller' => 'ProdutoCategorias', 'action' => 'view', $produto->produto_categoria->id]) : '' ?></td>
                    </tr>
                                                                                <tr>
                        <th>Id</th>
                        <td><?= $this->Number->format($produto->id) ?></td>
                    </tr>
                                                                                <tr>
                        <th>Destaque</th>
                        <td><?= $produto->destaque ? __('Yes') : __('No'); ?></td>
                     </tr>
                                        </table>
                                </div>
    </div>
    </div>
</div>
        <div class="col-md-3">
        <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
            <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Editar {0}', ['Produto']), ['action' => 'edit', $produto->id], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?> 
            <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $produto->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $produto->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
            ?>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Produto']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
            <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Produtos ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Produto Categorias']), ['controller' => 'ProdutoCategorias', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Produto Categoria']), ['controller' => 'ProdutoCategorias', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    </div>
    </div>
        </div>
</div>
