<?php $this->set("title","WM | Adcionar Usuário");  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","www.wmborrachaseacessorios.com.br");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<style type="text/css">
    .nav-usuarios{
        color: #FFFFFF !important;
    }
</style>
<div class="container">
    <div class="row">
        
        <div class="col-md-8 offset-md-1">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= 'Cadastrar Usuário' ?></h3>
            </div>
            <div class="box-body">
                <?= $this->Form->create($user) ?>
                <fieldset>
                    <?php
                                    echo $this->Form->input('email', ['class'=>'text-lowercase']);
                                    echo $this->Form->input('nome', ['class'=>'text-capitalize']);
                                    echo $this->Form->input('password');
                                ?>
                </fieldset>
                <button type="submit" class="btn btn-warning" style="margin-bottom: 20px;">Salvar</button>
                <?= $this->Form->end() ?>
            </div>
        </div>
        </div>
        <div class="col-md-3">
            <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Usuários ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                            </div>
        </div>
    </div>
</div>    
