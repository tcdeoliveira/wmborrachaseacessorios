<?= $this->fetch('css') ?>
<?= $this->Html->css('login.css') ?>
<?php $this->set("title","WM Borrchas e Acessórios");  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<?= $this->Form->create() ?>
<div class="container-fluid">
    <div class="row" >
        <div class="col-md-3 offset-md-3 slogan-ibhj" style="min-height: 250px; padding-top: 100px" >                   
            <div class="form-group">
            	<input type="email" name="email" class="form-control" placeholder="E-Mail">
            </div>
            <div class="form-group">
            	<input type="password" name="password" class="form-control" placeholder="Senha">
            </div>
            <button type="submit" class="btn btn-warning" >Login</button>
            <a class="btn btn-dark" href="<?php echo $this->Url->build('/') ?>">Home</a>
        </div>
    </div>
</div>
<?php echo  $this->Form->end(); ?>