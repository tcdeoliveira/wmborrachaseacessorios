
<!DOCTYPE html>
<html>
    <head>
        <meta name="rating" content="General">
        <meta name="AUTHOR" content="https://www.facebook.com/profile.php?id=100012578408914">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <!– Código do Schema.org também para o Google+ –>
        <meta itemprop=”name” content=”<?php echo "". $title; ?>“>
        <meta itemprop=”description” content=”<?php echo "". $sitedesc; ?>“>
        <meta itemprop=”image” content=”<?php echo "". $siteimage; ?>“>

        <!– Código do Open Graph –>
        <meta property="fb:app_id" content="1708650332774678" />
        <meta property="og:type" content="website">
        <meta property="og:locale" content="pt_BR">
        <meta property="og:url" content="<?php echo "". $page; ?>">
        <meta property="og:title" content="<?php echo "". $title; ?>">
        <meta property="og:site_name" content="WM BORRACHAS E ACESSORIOS">
        <meta property="og:description" content="<?php echo "". $sitedesc; ?>">
        <meta property="og:image" content="<?php echo "". $siteimage; ?>">
        <meta property="og:image:type" content="image/jpeg">
        <meta property="og:image:width" content="400"> 
        <meta property="og:image:height" content="400">    
        <meta name="revisit-after" content="1 days">
        <title>
            <?php echo "". $title; ?>
        </title>
        <?= $this->Html->charset() ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <title><?= $this->fetch('title') ?></title>    
        <?= $this->Html->meta('icon') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->Html->css('animate.css') ?>
        <?= $this->Html->css('bootstrap-reboot.css') ?>
        <?= $this->Html->css('bootstrap-grid.css') ?>
        <?= $this->Html->css('bootstrap.min.css') ?>   
        <?= $this->Html->css('home.css') ?>    
        <?= $this->Html->css('animate.css') ?>
        <?= $this->Html->script('jquery-3.2.1.slim.min.js') ?>
        <?= $this->Html->script('popper.min.js') ?>
        <?= $this->Html->script('bootstrap.min.js') ?>
    </head>
<body>
    <div class="body" >
        <header class="navbar navbar-expand-lg navbar-dark cor-menu" >
            <a class="navbar-brand" href="#">
                <?php echo $this->Html->image('wm.png', ['class'=>'menu-logo animated pulse']) ?>
            </a>
            <button style="" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <i class="material-icons">&#xE5D4;</i>
            </button>

            <div class="collapse navbar-collapse" id="navbarColor01">
                <?php
                    $default_nav_bar_left = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'nav-bar-left_1.ctp';
                    if (file_exists($default_nav_bar_left)) {
                        ob_start();
                        include $default_nav_bar_left;
                        echo ob_get_clean();
                    }
                    else {
                        echo $this->element('nav-bar-left_1');
                    }
                ?>
                <?php
                    $default_nav_bar_right = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'nav-bar-right_1.ctp';
                    if (file_exists($default_nav_bar_right)) {
                        ob_start();
                        include $default_nav_bar_right;
                        echo ob_get_clean();
                    }
                    else{
                        echo $this->element('nav-bar-right_1');
                    }
                ?> 
            </div>
        </header> 
        <div class="container">
            <?= $this->Flash->render() ?>
        </div>
        
        <?= $this->fetch('content') ?>
    </div>    
    <footer>
        <div class="footer1">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 offset-md-2">
                        <H4>Entre em contato <i class="material-icons">&#xE0BE;</i></H4>
                    </div>
                </div>
                <div class="row" id="contato">
                    
                    <div class="col-md-4 offset-md-2 ">
                    
                    
                        <?= $this->Form->create('contato', ['url'=>'/contatos/enviar']) ?>
                        
                        <div class="form-group">
                                <input  class="form-control animated flipInX" type="text" name="nome" placeholder="Qual o seu nome?">
                        </div>
                        <div class="form-group">
                            
                                <input class="form-control animated flipInX" type="email"name="email" placeholder="Informe um endereço de e-mail valido">
                            
                        </div>
                        
                        
                        
                       
                    
                </div>
                <div class="col-md-4 text-justify">
                    <div class="form-group">                           
                            <textarea  class="form-control animated flipInX" name="menssagem" placeholder="Digite sua menssagem" rows="4" ></textarea>                             
                        </div>
                    
                    
                    
                </div>
                
                </div>
                <div class="row">
                    <div class="col-md-4 offset-md-2">
                                            <button type="submit" class="btn btn-dark" style="margin-bottom: 20px">Enviar menssagem</button>

                    </div>
                </div>
                 <?= $this->Form->end() ?>
            </div>            
            <div class="container">
                <div class="text-center">
                    <small>
                    <h6>
                        WM ACESSÓRIOS PARA MOTOS - FÁBRICA
                    </h6>
                    <p>
                        Rua Bartolomeu Gusmão, Nº 35, Bom Sucesso, Fortaleza - CE
                    </p>
                    <p>(85) 3484-4873</p>
                    </small>
                    <small><b>Até aqui nos ajudou o Senhor!!!</b><i>1 Samuel 7:12</i></small>
                </div>
            </div>
        </div>        
        <div class="footer2">
                <div class="container text-center">
                    <a href="https://www.facebook.com/tcwebdev" target="blanck">
                    <?php echo $this->Html->image('BUZZDESIGN.png', ['class'=>'buzz-logo']) ?>
                    <br>
                    <i>Desenvolvido por </i><span style="color: #FFC107 !important">TcWebDev</span>
                    </a>
                </div>
        </div>
    </footer>
</body>
</html>
