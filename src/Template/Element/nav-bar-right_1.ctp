<?php 
                                echo $this->Form->create(null, [
                    'url' => ['controller' => 'Produtos', 'action' => 'search']
                ]);
                              ?>
                <div class="form-inline my-5 my-lg-0 busca">
                    <div class="input-group" style="margin-top:15px">
                      <input type="text" class="form-control" placeholder="O que você procura?" aria-label="Recipient's username" name="search" aria-describedby="basic-addon2">
                      <div class="input-group-append">
                        <button class="btn btn-warning btn-outline-warning-2" type="submit"><i class="material-icons">&#xE8B6;</i></button>
                      </div>
                    </div>
                    
                </div>  <?= $this->Form->end() ?> 
<?php
    if(isset($user_auth)): ?>
         <ul class="navbar-nav my-2 my-lg-0">
             <li class="nav-item active">
        <a class="nav-link" href="<?php echo $this->Url->build('/admcp') ?>">Admin</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $this->Url->build('/admcp/sair') ?>">Sair</a>
      </li>

    </ul>
<?php
    endif;
?>