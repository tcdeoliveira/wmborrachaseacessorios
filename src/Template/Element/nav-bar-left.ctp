<ul class="navbar-nav mr-auto">    
    <li class="nav-item">
        <a class="nav-link nav-produtos" href="<?php echo $this->Url->build('/admcp/produtos') ?>"><i class="material-icons">&#xE8CC;</i> PRODUTOS </a>
    </li>
    <li class="nav-item">
        <a class="nav-link nav-categorias" href="<?php echo $this->Url->build('/admcp/categorias') ?>"><i class="material-icons">&#xE8E7;</i> CATEGORIAS</a>
    </li>
    <li class="nav-item">
        <a class="nav-link nav-usuarios" href="<?php echo $this->Url->build('/admcp/usuarios') ?>"><i class="material-icons">&#xE853;</i> USUÁRIOS</a>
    </li>
    <li class="nav-item">
        <a class="nav-link nav-usuarios" href="<?php echo $this->Url->build('/') ?>">Voltar ao site</a>
    </li>
</ul>