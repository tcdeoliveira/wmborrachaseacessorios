<?= $this->fetch('css') ?>
<?= $this->Html->css('style.css') ?>
<?= $this->Html->css('font-awesome.css') ?>
<?php $this->set("title","WM Borrchas e Acessórios");  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<style type="text/css">
    .nav-produtos{
        color: #FFFFFF !important;
    }
    .link-categorias{
        color: #111111
    }
    .link-categorias:hover{
        color: #FFC107
    }
    .link-categorias-2{
        color: #FFFFFF !important;
    }
    .link-categorias-2:hover{
        color: #FFC107 !important;
    }
    
    .preencher-produtos{
      opacity: 0;
    }
    .veja-mais{
      background-color: #FFC107 !important;
      color: #111111;
    }
    a{
      color: #111111;
    }
    a:hover{
     
      color: #111111 !important;
      text-decoration: none;
    }
    a:hover{
     
      color: #111111 !important;
      text-decoration: none;
    }
    .categorias{

    }
    .categorias:hover{
      color: #FFC107 !important;
    }
</style>
<div class="container">
    <div class="row" style="padding-top:5%;padding-bottom:5%">
        <div class="col-md-3 ">
            <div id="accordion">
                <div class="card  bg-warning mb-3" style="max-width: 18rem;">
                      <div class="card-header">
                           
                                <a class="link-categorias" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  <b>Categorias</b>
                                </a>
                              
                      </div>
                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                      <div class="card-body">
                        
                        <?php foreach ($categorias as $categoria): ?>
                            <a href="<?php echo $this->Url->build('/produtos/categorias/'.$categoria->id.'/'.$categoria->nome); ?>" class="badge badge-dark link-categorias-2"><?php echo $categoria['nome']; ?></a>
                        <?php endforeach; ?>
                      </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">                  
          <?php $renew = 0; ?>
          <div class="card-deck">
            <?php foreach ($produtos as $produto): ?>
              <?PHP $renew++; ?> 
              <?php if($renew>3): $renew = 1;?>
              </div>
              <br>
              <div class="card-deck">
              <?php endif; ?>
              <div class="card">
                <img class="card-img-top" src="<?php echo $this->Url->build("/img/produtos/$produto->foto"); ?>" alt="<?php echo $produto->nome ?>" alt="" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title"><?php echo $produto->nome ?></h5>
                </div>
                <a href="<?php echo $this->Url->build('/produtos/detalhes/'.$produto->url) ?>">
                  <div class="card-footer veja-mais btn-warning">
                    Veja mais
                  </div>
                </a>
              </div>                     
            <?php endforeach; ?>
            <?php switch ($renew) {
              case 1: ?>
                <div class="card preencher-produtos" ></div>
                <div class="card preencher-produtos" ></div>
                <?php break;
              case 2: ?>
                <div class="card preencher-produtos" ></div>
                <?php break;              
            } ?>
           </div>
           <br>
           <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <?= $this->Paginator->prev('&laquo; ' . __(''), ['escape'=>false]) ?>
                                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                                <?= $this->Paginator->next(__('') . ' &raquo;', ['escape'=>false]) ?>

                            </ul>
                            <p  class="pagination justify-content-end pagination-text" style="color: white !important"><?= $this->Paginator->counter(__('Página {{page}} de {{pages}}, exibindo {{current}} registros de
                            {{count}} encontrados.')) ?></p>
                        </nav>          
        </div>
    </div>   
</div>
