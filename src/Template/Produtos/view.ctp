<?= $this->fetch('css') ?>
<?php $this->set("title","WM | ".$produto->nome);  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<div class="container-fluid "style="background-color: #F8F8F8 !important; min-heigth: 300px; padding-top:5%;padding-bottom:5%">
<div class="container ">
    <div class="col-md-12">
        <div class="row"><h2><?php echo $produto->nome ?></h2></div>
    </div>
    <div class="row" >    
        <div class="col-md-3">
            <div class="card text-white bg-warning" >
              <img class="card-img-top" src="<?php echo $this->Url->build("/img/produtos/$produto->foto"); ?>" alt="<?php echo $produto->nome ?>">
              
            </div>
        </div> 
        <div class="col-md-9">
            <p>
                <?php echo $produto->descrisao ?>
            </p>  
            <a href="<?php echo $this->Url->build('/produtos/categorias/'.$produto->produto_categoria['id'].'/'.$produto->produto_categoria['nome']) ?>" class="badge badge-dark link-categorias-2"><?php echo $produto->produto_categoria['nome']; ?></a>  
        </div>
        </div>

    </div>
</div>
