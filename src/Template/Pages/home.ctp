<?= $this->fetch('css') ?>
<?= $this->Html->css('homepage.css') ?>
<?php $this->set("title","WM Borrchas e Acessórios");  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<div class="container-fluid ">
    <div class="row home" style="padding-top:10%">
        <div class="col-md-6 offset-md-3 animated fadeInLeft"  >                   
              <?php 
                echo $this->Form->create(null, [
    'url' => ['controller' => 'Produtos', 'action' => 'search']
]);
              ?>
                    <div class="input-group" style="min-width: 100% !important">
                      <input type="text" class="form-control" placeholder="O que você procura?" aria-label="Recipient's username" aria-describedby="basic-addon2" name="search">
                      <div class="input-group-append">
                        <button class="btn btn-warning btn-outline-warning-2" type="submit"><i class="material-icons">&#xE8B6;</i></button>
                      </div>
                    </div>
              <?= $this->Form->end() ?>
        </div>
    </div>
</div>