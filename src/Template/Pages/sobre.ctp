<?= $this->fetch('css') ?>
<?= $this->Html->css('sobre.css') ?>
<?php $this->set("title","WM Borrchas e Acessórios | Quem Somos");  ?>  
<?php $this->set("page","http://www.wmborrachaseacessorios.com.br");  ?>  
<?php $this->set("keywords","WM, acessórios, moto, borrachas");  ?>  
<?php $this->set("sitedesc","");  ?> 
<?php $this->set("siteimage","http://wmborrachaseacessorios.com.br/img/wm.png");  ?>
<div class="container-fluid sobre ">
    <div class="container">
        <div class="row" style="padding-top:10%; padding-bottom: 10%">
            <div class="col-md-6 text-center">
                <img src="<?php echo $this->url->build('img/wm2.png') ?>" class="img-fluid animated fadeInLeft">
            </div>
            <div class="col-md-6 animated ">
                <br>
                <h3 class="animated flipInX">Quem somos?</h3>
                <p class="animated fadeInUp">Somos uma empresa fabricante de componentes e acessórios para motos.</p>
                <p class="animated fadeInUp">Nosso trabalho teve seu inicio em 2010 com a fundação da WM,tendo como objtivo inovar o
                comercio com seus produtos de qualidade</p>
            </div>
        </div>
    </div>
</div>