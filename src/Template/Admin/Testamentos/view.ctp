<div class="container">
    <div class="row">
    
    <div class="col-md-9">
        <div class="testamentos col-lg-10 col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-right"><?= h($testamento->id) ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover">
                                                        <tr>
                        <th>Id</th>
                        <td><?= h($testamento->id) ?></td>
                    </tr>
                                                        <tr>
                        <th>Nome</th>
                        <td><?= h($testamento->nome) ?></td>
                    </tr>
                                                                                        </table>
                                        <div class="related">
                    <?php if (!empty($testamento->livros)): ?>
                    <h4><?= __('Related {0}', ['Livros']) ?></h4>
                    <table class="table table-striped table-hover">
                        <tr>
                                        <th>Id</th>
                                        <th>Testamento Id</th>
                                        <th>Posicao</th>
                                        <th>Nome</th>
                                        <th>Abreviado</th>
                                        <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($testamento->livros as $livros): ?>
                        <tr>
                            <td><?= h($livros->id) ?></td>
                            <td><?= h($livros->testamento_id) ?></td>
                            <td><?= h($livros->posicao) ?></td>
                            <td><?= h($livros->nome) ?></td>
                            <td><?= h($livros->abreviado) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Livros', 'action' => 'view', $livros->id]) ?>

                                <?= $this->Html->link(__('Edit'), ['controller' => 'Livros', 'action' => 'edit', $livros->id]) ?>

                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Livros', 'action' => 'delete', $livros->id], ['confirm' => __('Are you sure you want to delete # {0}?', $livros->id)]) ?>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php endif; ?>
                </div>
                </div>
    </div>
    </div>
</div>
        <div class="col-md-3">
        <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
            <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Editar {0}', ['Testamento']), ['action' => 'edit', $testamento->id], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?> 
            <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $testamento->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $testamento->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
            ?>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Testamento']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
            <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Testamentos ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Livros']), ['controller' => 'Livros', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Livro']), ['controller' => 'Livros', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    </div>
    </div>
        </div>
</div>
