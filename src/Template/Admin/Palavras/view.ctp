<div class="container">
    <div class="row">
    
    <div class="col-md-9">
        <div class="palavras col-lg-10 col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-right"><?= h($palavra->id) ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover">
                                                        <tr>
                        <th>Titulo</th>
                        <td><?= h($palavra->titulo) ?></td>
                    </tr>
                                                        <tr>
                        <th>Url</th>
                        <td><?= h($palavra->url) ?></td>
                    </tr>
                                                        <tr>
                        <th>Pregador</th>
                        <td><?= h($palavra->pregador) ?></td>
                    </tr>
                                                        <tr>
                        <th>Congregado</th>
                        <td><?= $palavra->has('congregado') ? $this->Html->link($palavra->congregado->id, ['controller' => 'Congregados', 'action' => 'view', $palavra->congregado->id]) : '' ?></td>
                    </tr>
                                                                                <tr>
                        <th>Id</th>
                        <td><?= $this->Number->format($palavra->id) ?></td>
                    </tr>
                                                                    <tr>
                        <th>Data</th>
                        <td><?= h($palavra->data) ?></tr>
                    </tr>
                                <tr>
                        <th>Created</th>
                        <td><?= h($palavra->created) ?></tr>
                    </tr>
                                                    </table>
                                        <div class="row">
                    <h4>Texto</h4>
                    <?= $this->Text->autoParagraph(h($palavra->texto)); ?>
                </div>
                                            </div>
    </div>
    </div>
</div>
        <div class="col-md-3">
        <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
            <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Editar {0}', ['Palavra']), ['action' => 'edit', $palavra->id], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?> 
            <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $palavra->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $palavra->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
            ?>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Palavra']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
            <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Palavras ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Congregados']), ['controller' => 'Congregados', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Congregado']), ['controller' => 'Congregados', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    </div>
    </div>
        </div>
</div>
