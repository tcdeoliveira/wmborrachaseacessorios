<div class="container">
    <div class="row">
        
        <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= 'Edit Palavra' ?></h3>
            </div>
            <div class="box-body">
                <?= $this->Form->create($palavra) ?>
                <fieldset>
                    <?php
                                    echo $this->Form->input('titulo');
                                    echo $this->Form->input('url');
                                    echo $this->Form->input('data');
                                    echo $this->Form->input('texto');
                                    echo $this->Form->input('pregador');
                                    echo $this->Form->input('user_id', ['options' => $congregados]);
                                ?>
                </fieldset>
                <button type="submit" class="btn btn-ibhj-1" style="margin-bottom: 20px;">Salvar</button>
                <?= $this->Form->end() ?>
            </div>
        </div>
        </div>
        <div class="col-md-3">
            <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
                                    <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $palavra->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $palavra->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
                    ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Palavras ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', 'Congregados'), ['controller' => 'Congregados', 'action' => 'index']) ?>

                <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', 'Congregado'), ['controller' => 'Congregados', 'action' => 'add']) ?>

                            </div>
        </div>
    </div>
</div>    
