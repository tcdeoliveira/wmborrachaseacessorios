<div class="container">
    <div class="row">
    
    <div class="col-md-9">
        <div class="livros col-lg-10 col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-right"><?= h($livro->id) ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover">
                                                        <tr>
                        <th>Id</th>
                        <td><?= h($livro->id) ?></td>
                    </tr>
                                                        <tr>
                        <th>Testamento Id</th>
                        <td><?= h($livro->testamento_id) ?></td>
                    </tr>
                                                        <tr>
                        <th>Posicao</th>
                        <td><?= h($livro->posicao) ?></td>
                    </tr>
                                                        <tr>
                        <th>Nome</th>
                        <td><?= h($livro->nome) ?></td>
                    </tr>
                                                        <tr>
                        <th>Abreviado</th>
                        <td><?= h($livro->abreviado) ?></td>
                    </tr>
                                                                                        </table>
                                        <div class="related">
                    <?php if (!empty($livro->versiculos)): ?>
                    <h4><?= __('Related {0}', ['Versiculos']) ?></h4>
                    <table class="table table-striped table-hover">
                        <tr>
                                        <th>Id</th>
                                        <th>Version Id</th>
                                        <th>Livro Id</th>
                                        <th>Capitulo</th>
                                        <th>Versiculo</th>
                                        <th>Texto</th>
                                        <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($livro->versiculos as $versiculos): ?>
                        <tr>
                            <td><?= h($versiculos->id) ?></td>
                            <td><?= h($versiculos->version_id) ?></td>
                            <td><?= h($versiculos->livro_id) ?></td>
                            <td><?= h($versiculos->capitulo) ?></td>
                            <td><?= h($versiculos->versiculo) ?></td>
                            <td><?= h($versiculos->texto) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Versiculos', 'action' => 'view', $versiculos->id]) ?>

                                <?= $this->Html->link(__('Edit'), ['controller' => 'Versiculos', 'action' => 'edit', $versiculos->id]) ?>

                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Versiculos', 'action' => 'delete', $versiculos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $versiculos->id)]) ?>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php endif; ?>
                </div>
                </div>
    </div>
    </div>
</div>
        <div class="col-md-3">
        <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
            <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Editar {0}', ['Livro']), ['action' => 'edit', $livro->id], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?> 
            <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $livro->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $livro->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
            ?>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Livro']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
            <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Livros ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Versiculos']), ['controller' => 'Versiculos', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Versiculo']), ['controller' => 'Versiculos', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    </div>
    </div>
        </div>
</div>
