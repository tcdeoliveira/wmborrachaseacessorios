<div class="container">
    <div class="row">
    
    <div class="col-md-9">
        <div class="textos col-lg-10 col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-right"><?= h($texto->id) ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover">
                                                        <tr>
                        <th>Titulo</th>
                        <td><?= h($texto->titulo) ?></td>
                    </tr>
                                                        <tr>
                        <th>Autor Texto</th>
                        <td><?= $texto->has('autor_texto') ? $this->Html->link($texto->autor_texto->id, ['controller' => 'AutorTextos', 'action' => 'view', $texto->autor_texto->id]) : '' ?></td>
                    </tr>
                                                        <tr>
                        <th>Foto</th>
                        <td><?= h($texto->foto) ?></td>
                    </tr>
                                                        <tr>
                        <th>Url</th>
                        <td><?= h($texto->url) ?></td>
                    </tr>
                                                        <tr>
                        <th>Autor</th>
                        <td><?= h($texto->autor) ?></td>
                    </tr>
                                                                                <tr>
                        <th>Id</th>
                        <td><?= $this->Number->format($texto->id) ?></td>
                    </tr>
                                                                    <tr>
                        <th>Data</th>
                        <td><?= h($texto->data) ?></tr>
                    </tr>
                                <tr>
                        <th>Created</th>
                        <td><?= h($texto->created) ?></tr>
                    </tr>
                                                                    <tr>
                        <th>Ativo</th>
                        <td><?= $texto->ativo ? __('Yes') : __('No'); ?></td>
                     </tr>
                                        </table>
                                        <div class="row">
                    <h4>Texto</h4>
                    <?= $this->Text->autoParagraph(h($texto->texto)); ?>
                </div>
                                            </div>
    </div>
    </div>
</div>
        <div class="col-md-3">
        <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
            <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Editar {0}', ['Texto']), ['action' => 'edit', $texto->id], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?> 
            <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $texto->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $texto->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
            ?>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Texto']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
            <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Textos ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Autor Textos']), ['controller' => 'AutorTextos', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Autor Texto']), ['controller' => 'AutorTextos', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    </div>
    </div>
        </div>
</div>
