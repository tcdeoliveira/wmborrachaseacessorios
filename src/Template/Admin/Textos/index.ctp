
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group" id="list-tab" role="tablist" style="margin-bottom: 20px;">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Textos</a>
                <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Texto']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                                    <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Autor Textos']), ['controller' => 'AutorTextos', 'action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo  {0}', ['Autor Texto <i class="material-icons">add_circle</i>']), ['controller' => 'AutorTextos', 'action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content" id="nav-tabContent">
                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table  table-sm table-hover table-striped table-responsive-sm border-cor-ibhj">
                            <thead class="bg-cor-ibhj">
                                <tr>
                                                                            <th><?= $this->Paginator->sort('id') ?></th>
                                                                            <th><?= $this->Paginator->sort('titulo') ?></th>
                                                                            <th><?= $this->Paginator->sort('autor_id') ?></th>
                                                                            <th><?= $this->Paginator->sort('data') ?></th>
                                                                            <th><?= $this->Paginator->sort('created') ?></th>
                                                                            <th><?= $this->Paginator->sort('foto') ?></th>
                                                                            <th><?= $this->Paginator->sort('url') ?></th>
                                                                        <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($textos as $texto): ?>
                                        <tr>
                                                                                                <td><?= $this->Number->format($texto->id) ?></td>
                                                                                            <td>
                                                <?= h($texto->titulo) ?></td>
                                                                                            <td>
                                                <?= $texto->has('autor_texto') ? $this->Html->link($texto->autor_texto->id, ['controller' => 'AutorTextos', 'action' => 'view', $texto->autor_texto->id]) : '' ?>
                                            </td>
                                                                                        <td>
                                                <?= h($texto->data) ?></td>
                                                                                            <td>
                                                <?= h($texto->created) ?></td>
                                                                                            <td>
                                                <?= h($texto->foto) ?></td>
                                                                                            <td>
                                                <?= h($texto->url) ?></td>
                                                                                            <td class="actions" style="white-space:nowrap">
                                                <?= $this->Html->link(__('<i class="material-icons  btn-pequeno">&#xE5C6;</i>'), ['action' => 'view', $texto->id], ['class'=>'btn btn btn-outline-secondary btn-sm btn-pequeno', 'escape'=>false, 'title'=>'Exibir']) ?>
                                                <?= $this->Html->link(__('<i class="material-icons  btn-pequeno">&#xE644;</i>'), ['action' => 'edit', $texto->id], ['class'=>'btn btn btn-outline-secondary btn-sm  btn-pequeno', 'escape'=>false, 'title'=>'Editar']) ?>
                                                <?= $this->Form->postLink(__('<i class="material-icons  btn-pequeno">&#xE92B;</i>'), ['action' => 'delete', $texto->id], ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $texto->id), 'class'=>'btn btn-danger btn-sm  btn-pequeno', 'escape'=>false, 'title'=>'Excluir']) ?>
                                            </td>
                                        </tr>
                                <?php 
                                    endforeach; 
                                ?>
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <?= $this->Paginator->prev('&laquo; ' . __(''), ['escape'=>false]) ?>
                                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                                <?= $this->Paginator->next(__('') . ' &raquo;', ['escape'=>false]) ?>

                            </ul>
                            <p  class="pagination justify-content-end pagination-text"><?= $this->Paginator->counter(__('Página {{page}} de {{pages}}, exibindo {{current}} registros de
                            {{count}} encontrados.')) ?></p>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
