<div class="container">
    <div class="row">
        
        <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= 'Edit Versiculo' ?></h3>
            </div>
            <div class="box-body">
                <?= $this->Form->create($versiculo) ?>
                <fieldset>
                    <?php
                                    echo $this->Form->input('version_id');
                                    echo $this->Form->input('livro_id');
                                    echo $this->Form->input('capitulo');
                                    echo $this->Form->input('versiculo');
                                    echo $this->Form->input('texto');
                                ?>
                </fieldset>
                <button type="submit" class="btn btn-ibhj-1" style="margin-bottom: 20px;">Salvar</button>
                <?= $this->Form->end() ?>
            </div>
        </div>
        </div>
        <div class="col-md-3">
            <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
                                    <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $versiculo->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $versiculo->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
                    ?>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Versiculos ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                            </div>
        </div>
    </div>
</div>    
