
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group" id="list-tab" role="tablist" style="margin-bottom: 20px;">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Versiculos</a>
                <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Versiculo']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content" id="nav-tabContent">
                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table  table-sm table-hover table-striped table-responsive-sm border-cor-ibhj">
                            <thead class="bg-cor-ibhj">
                                <tr>
                                                                            <th><?= $this->Paginator->sort('id') ?></th>
                                                                            <th><?= $this->Paginator->sort('version_id') ?></th>
                                                                            <th><?= $this->Paginator->sort('livro_id') ?></th>
                                                                            <th><?= $this->Paginator->sort('capitulo') ?></th>
                                                                            <th><?= $this->Paginator->sort('versiculo') ?></th>
                                                                        <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($versiculos as $versiculo): ?>
                                        <tr>
                                                                                                <td><?= $this->Number->format($versiculo->id) ?></td>
                                                                                            <td>
                                                <?= h($versiculo->version_id) ?></td>
                                                                                            <td>
                                                <?= h($versiculo->livro_id) ?></td>
                                                                                            <td>
                                                <?= h($versiculo->capitulo) ?></td>
                                                                                            <td>
                                                <?= h($versiculo->versiculo) ?></td>
                                                                                            <td class="actions" style="white-space:nowrap">
                                                <?= $this->Html->link(__('<i class="material-icons  btn-pequeno">&#xE5C6;</i>'), ['action' => 'view', $versiculo->id], ['class'=>'btn btn btn-outline-secondary btn-sm btn-pequeno', 'escape'=>false, 'title'=>'Exibir']) ?>
                                                <?= $this->Html->link(__('<i class="material-icons  btn-pequeno">&#xE644;</i>'), ['action' => 'edit', $versiculo->id], ['class'=>'btn btn btn-outline-secondary btn-sm  btn-pequeno', 'escape'=>false, 'title'=>'Editar']) ?>
                                                <?= $this->Form->postLink(__('<i class="material-icons  btn-pequeno">&#xE92B;</i>'), ['action' => 'delete', $versiculo->id], ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $versiculo->id), 'class'=>'btn btn-danger btn-sm  btn-pequeno', 'escape'=>false, 'title'=>'Excluir']) ?>
                                            </td>
                                        </tr>
                                <?php 
                                    endforeach; 
                                ?>
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <?= $this->Paginator->prev('&laquo; ' . __(''), ['escape'=>false]) ?>
                                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                                <?= $this->Paginator->next(__('') . ' &raquo;', ['escape'=>false]) ?>

                            </ul>
                            <p  class="pagination justify-content-end pagination-text"><?= $this->Paginator->counter(__('Página {{page}} de {{pages}}, exibindo {{current}} registros de
                            {{count}} encontrados.')) ?></p>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
