<div class="container">
    <div class="row">
    
    <div class="col-md-9">
        <div class="versiculos col-lg-10 col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-right"><?= h($versiculo->id) ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover">
                                                        <tr>
                        <th>Version Id</th>
                        <td><?= h($versiculo->version_id) ?></td>
                    </tr>
                                                        <tr>
                        <th>Livro Id</th>
                        <td><?= h($versiculo->livro_id) ?></td>
                    </tr>
                                                        <tr>
                        <th>Capitulo</th>
                        <td><?= h($versiculo->capitulo) ?></td>
                    </tr>
                                                        <tr>
                        <th>Versiculo</th>
                        <td><?= h($versiculo->versiculo) ?></td>
                    </tr>
                                                                                <tr>
                        <th>Id</th>
                        <td><?= $this->Number->format($versiculo->id) ?></td>
                    </tr>
                                                                </table>
                                        <div class="row">
                    <h4>Texto</h4>
                    <?= $this->Text->autoParagraph(h($versiculo->texto)); ?>
                </div>
                                            </div>
    </div>
    </div>
</div>
        <div class="col-md-3">
        <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
            <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Editar {0}', ['Versiculo']), ['action' => 'edit', $versiculo->id], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?> 
            <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $versiculo->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $versiculo->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
            ?>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Versiculo']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
            <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Versiculos ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    </div>
    </div>
        </div>
</div>
