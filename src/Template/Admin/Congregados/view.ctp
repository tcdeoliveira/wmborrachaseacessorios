<div class="container">
    <div class="row">
    
    <div class="col-md-9">
        <div class="congregados col-lg-10 col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-right"><?= h($congregado->id) ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-hover">
                                                        <tr>
                        <th>Nome</th>
                        <td><?= h($congregado->nome) ?></td>
                    </tr>
                                                        <tr>
                        <th>Sobrenome</th>
                        <td><?= h($congregado->sobrenome) ?></td>
                    </tr>
                                                        <tr>
                        <th>Foto</th>
                        <td><?= h($congregado->foto) ?></td>
                    </tr>
                                                        <tr>
                        <th>Apelido</th>
                        <td><?= h($congregado->apelido) ?></td>
                    </tr>
                                                        <tr>
                        <th>Email</th>
                        <td><?= h($congregado->email) ?></td>
                    </tr>
                                                        <tr>
                        <th>Ano</th>
                        <td><?= h($congregado->ano) ?></td>
                    </tr>
                                                        <tr>
                        <th>Password</th>
                        <td><?= h($congregado->password) ?></td>
                    </tr>
                                                        <tr>
                        <th>Telefone1 Ddd</th>
                        <td><?= h($congregado->telefone1_ddd) ?></td>
                    </tr>
                                                        <tr>
                        <th>Telefone1 Numero</th>
                        <td><?= h($congregado->telefone1_numero) ?></td>
                    </tr>
                                                        <tr>
                        <th>Telefone1 Tipo</th>
                        <td><?= h($congregado->telefone1_tipo) ?></td>
                    </tr>
                                                        <tr>
                        <th>Telefone2 Ddd</th>
                        <td><?= h($congregado->telefone2_ddd) ?></td>
                    </tr>
                                                        <tr>
                        <th>Telefone2 Numero</th>
                        <td><?= h($congregado->telefone2_numero) ?></td>
                    </tr>
                                                        <tr>
                        <th>Telefone2 Tipo</th>
                        <td><?= h($congregado->telefone2_tipo) ?></td>
                    </tr>
                                                        <tr>
                        <th>Cep</th>
                        <td><?= h($congregado->cep) ?></td>
                    </tr>
                                                        <tr>
                        <th>Rua</th>
                        <td><?= h($congregado->rua) ?></td>
                    </tr>
                                                        <tr>
                        <th>Numero</th>
                        <td><?= h($congregado->numero) ?></td>
                    </tr>
                                                        <tr>
                        <th>Bairro</th>
                        <td><?= h($congregado->bairro) ?></td>
                    </tr>
                                                        <tr>
                        <th>Cidade</th>
                        <td><?= h($congregado->cidade) ?></td>
                    </tr>
                                                        <tr>
                        <th>Estado</th>
                        <td><?= h($congregado->estado) ?></td>
                    </tr>
                                                        <tr>
                        <th>Complemento</th>
                        <td><?= h($congregado->complemento) ?></td>
                    </tr>
                                                                                <tr>
                        <th>Id</th>
                        <td><?= $this->Number->format($congregado->id) ?></td>
                    </tr>
                                <tr>
                        <th>Sexo</th>
                        <td><?= $this->Number->format($congregado->sexo) ?></td>
                    </tr>
                                <tr>
                        <th>Tipo</th>
                        <td><?= $this->Number->format($congregado->tipo) ?></td>
                    </tr>
                                <tr>
                        <th>Regra</th>
                        <td><?= $this->Number->format($congregado->regra) ?></td>
                    </tr>
                                                                    <tr>
                        <th>Data Nsc</th>
                        <td><?= h($congregado->data_nsc) ?></tr>
                    </tr>
                                <tr>
                        <th>Created</th>
                        <td><?= h($congregado->created) ?></tr>
                    </tr>
                                <tr>
                        <th>Modified</th>
                        <td><?= h($congregado->modified) ?></tr>
                    </tr>
                                                    </table>
                                        <div class="row">
                    <h4>Mapa</h4>
                    <?= $this->Text->autoParagraph(h($congregado->mapa)); ?>
                </div>
                                            </div>
    </div>
    </div>
</div>
        <div class="col-md-3">
        <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
            <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Editar {0}', ['Congregado']), ['action' => 'edit', $congregado->id], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?> 
            <?= $this->Form->postLink(__('<i class="material-icons">&#xE92B;</i>Deletar'), 
                            ['action' => 'delete', $congregado->id], 
                            ['confirm' => __('Você está certo de que quer deletar o registro: # {0}?', $congregado->id), 'class'=>'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false, 'title'=>'Excluir']) 
            ?>
            <?= $this->Html->link(__('<i class="material-icons">add_circle</i> Novo {0}', ['Congregado']), ['action' => 'add'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
            <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Congregados ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                    </div>
    </div>
        </div>
</div>
