<div class="container">
    <div class="row">
        
        <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= 'Add Congregado' ?></h3>
            </div>
            <div class="box-body">
                <?= $this->Form->create($congregado) ?>
                <fieldset>
                    <?php
                                    echo $this->Form->input('nome');
                                    echo $this->Form->input('sobrenome');
                                    echo $this->Form->input('foto');
                                    echo $this->Form->input('apelido');
                                    echo $this->Form->input('email');
                                    echo $this->Form->input('data_nsc');
                                    echo $this->Form->input('ano');
                                    echo $this->Form->input('sexo');
                                    echo $this->Form->input('tipo');
                                    echo $this->Form->input('password');
                                    echo $this->Form->input('regra');
                                    echo $this->Form->input('telefone1_ddd');
                                    echo $this->Form->input('telefone1_numero');
                                    echo $this->Form->input('telefone1_tipo');
                                    echo $this->Form->input('telefone2_ddd');
                                    echo $this->Form->input('telefone2_numero');
                                    echo $this->Form->input('telefone2_tipo');
                                    echo $this->Form->input('cep');
                                    echo $this->Form->input('rua');
                                    echo $this->Form->input('numero');
                                    echo $this->Form->input('bairro');
                                    echo $this->Form->input('cidade');
                                    echo $this->Form->input('estado');
                                    echo $this->Form->input('complemento');
                                    echo $this->Form->input('mapa');
                                ?>
                </fieldset>
                <button type="submit" class="btn btn-ibhj-1" style="margin-bottom: 20px;">Salvar</button>
                <?= $this->Form->end() ?>
            </div>
        </div>
        </div>
        <div class="col-md-3">
            <div class="list-group" id="list-tab" style="margin-bottom: 20px;" role="tablist">
                <a class="list-group-item list-group-item-action active bg-cor-ibhj border-cor-ibhj" style="" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Ações</a>
                                <?= $this->Html->link(__('<i class="material-icons">list</i> Listar {0}', ['Congregados ']), ['action' => 'index'], ['class' => 'list-group-item list-group-item-action   border-cor-ibhj', 'escape'=>false]) ?>
                            </div>
        </div>
    </div>
</div>    
