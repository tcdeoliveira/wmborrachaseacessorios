
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="https://fonts.googleapis.com/css?family=Oswald|Raleway" rel="stylesheet">
    </head>
<body>
<div class="" style="display: flex; padding:2%; background-color: rgb(28,125,181); font-family: 'Oswald', sans-serif;">
    
    <h1 class="" style="text-align: center; color: white !important; text-shadow: 2px 2px black; line-height: 20px !important; font-size: 2em; width: 100% !important; text-align: center !important;  ">
        Olá, Menssagem recebida de <?php echo $nome ?>.
    </h1>      
</div>

<div style=" background-color: #EBECEC; padding: 5%; margin: 0; font-family: 'Raleway', sans-serif; font-size: 1.5em">
    <p style="color:black;">
        <?php echo $menssagem ?>
    </p> 
    <hr>
    <small>
        <p>
            De: <?php echo $nome ?>
        </p>
        <p>
            E-mail: <?php echo $email ?>
        </p>
    </small></small>
</div>
</body>
</html>