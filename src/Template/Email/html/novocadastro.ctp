
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="https://fonts.googleapis.com/css?family=Oswald|Raleway" rel="stylesheet">
    </head>
<body>
<div class="" style="display: flex; padding:2%; background-color: rgb(28,125,181); font-family: 'Oswald', sans-serif;">
    
    <h1 class="" style="text-align: center; color: white !important; text-shadow: 2px 2px black; line-height: 20px !important; font-size: 2em; width: 100% !important; text-align: center !important;  ">
        <?php echo $cad['nome'] ?> Cadastrou-se no site.
    </h1>      
</div>

<div style=" background-color: #EBECEC; padding: 5%; margin: 0; font-family: 'Raleway', sans-serif; font-size: 1.5em">
    <p style="color:black;">
        Um novo congregado fez o seu cadastro no site. É preciso conferir e validar as informações desse novo cadastro.
    </p>

    <p>
        Clique no link para ver mais detalhes de <a href="http://www.ibhj.com.br/admin/congregado/<?php echo $cad['id'] ?>"><?php $cad['nome']." ".$cad['sobrenome'] ?></a>
    </p>
</div>
</body>
</html>