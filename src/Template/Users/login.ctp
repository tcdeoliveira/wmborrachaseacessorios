<?= $this->fetch('css') ?>
<?= $this->Html->css('cadastro.css') ?>
<?php $this->set("title","IBHJ | Login");  ?>  
<?php $this->set("page","http://www.ibhj.com.br/entrar");  ?>  
<?php $this->set("keywords","IBHJ, igreja, batista, igreja batista, igreja batista de henrique jorge, henrique, jorge, henrique jorge, jesus, religião, salvação, cuidando, vidas, amor, cuidando de vidas em amor, igreja evangelica, evangelica, bíblia");  ?>  
<?php $this->set("sitedesc","Igreja Batista de Henrique Jorge, ha 47 anos, cuidando de vidas em amor. Localizada na Rua Porto Algre, 997, Henrique Jorge, Fortaleza - CE, com cultos de oração as Quartas-Feiras a partir de 19h, "
        . "escola bíblica dominical aos Domingos de 9h às 10:20h e cultos de adoração aos domingos pela manhã das 10:30h as 11:30h e anoite das 19h às 21h.");  ?> 
<?php $this->set("siteimage","http://ibhj.com.br/img/ibhj.JPG");  ?>
<link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace|Rock+Salt|Sarina" rel="stylesheet">     
<div class="cadastro">
    <div class="container">
        <div class="col-md-4 push-md-4" style="padding-top: 5%; padding-bottom: 5%">
            <?= $this->Form->create() ?>
                <h4 class="animated fadeIn" style="color:rgb(28,125,181); text-align: center; font-family: JuliusSansOne-Regular;">ACESSO RESTRITO</h4>
                <div class="form-group">
                    <input type="email" class="form-control animated fadeInDown" name="email" placeholder="E-Mail">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control animated fadeInUp" name="password" placeholder="Senha">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default animated fadeIn" style="cursor: pointer; width: 100%">Entrar</button>
                </div>
            </p>
            <p style="text-align: center">
                <a href="#" style="color: rgb(28,125,181) !important;"><smal style="text-align: center"> Esqueci minha senha</smal></a>
            </p>
            
        </div>
    </div>
</div>
