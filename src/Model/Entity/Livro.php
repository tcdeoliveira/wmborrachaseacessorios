<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Livro Entity
 *
 * @property int $id
 * @property int $testamento_id
 * @property int $posicao
 * @property string $nome
 * @property string $abreviado
 *
 * @property \App\Model\Entity\Testamento $testamento
 * @property \App\Model\Entity\Versiculo[] $versiculos
 */
class Livro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'testamento_id' => true,
        'posicao' => true,
        'nome' => true,
        'abreviado' => true,
        'testamento' => true,
        'versiculos' => true
    ];
}
