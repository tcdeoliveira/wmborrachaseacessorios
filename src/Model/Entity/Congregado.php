<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Congregado Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $sobrenome
 * @property string $foto
 * @property string $apelido
 * @property string $email
 * @property \Cake\I18n\FrozenDate $data_nsc
 * @property string $ano
 * @property int $sexo
 * @property int $tipo
 * @property string $password
 * @property int $regra
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $telefone1_ddd
 * @property string $telefone1_numero
 * @property string $telefone1_tipo
 * @property string $telefone2_ddd
 * @property string $telefone2_numero
 * @property string $telefone2_tipo
 * @property string $cep
 * @property string $rua
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property string $complemento
 * @property string $mapa
 */
class Congregado extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'sobrenome' => true,
        'foto' => true,
        'apelido' => true,
        'email' => true,
        'data_nsc' => true,
        'ano' => true,
        'sexo' => true,
        'tipo' => true,
        'password' => true,
        'regra' => true,
        'created' => true,
        'modified' => true,
        'telefone1_ddd' => true,
        'telefone1_numero' => true,
        'telefone1_tipo' => true,
        'telefone2_ddd' => true,
        'telefone2_numero' => true,
        'telefone2_tipo' => true,
        'cep' => true,
        'rua' => true,
        'numero' => true,
        'bairro' => true,
        'cidade' => true,
        'estado' => true,
        'complemento' => true,
        'mapa' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
