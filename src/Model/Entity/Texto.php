<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Texto Entity
 *
 * @property int $id
 * @property string $titulo
 * @property string $texto
 * @property int $autor_id
 * @property \Cake\I18n\FrozenDate $data
 * @property \Cake\I18n\FrozenTime $created
 * @property string $foto
 * @property string $url
 * @property bool $ativo
 * @property string $autor
 *
 * @property \App\Model\Entity\AutorTexto $autor_texto
 */
class Texto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'titulo' => true,
        'texto' => true,
        'autor_id' => true,
        'data' => true,
        'created' => true,
        'foto' => true,
        'url' => true,
        'ativo' => true,
        'autor' => true,
        'autor_texto' => true
    ];
}
