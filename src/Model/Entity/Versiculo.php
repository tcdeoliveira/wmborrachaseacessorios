<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Versiculo Entity
 *
 * @property int $id
 * @property int $version_id
 * @property int $livro_id
 * @property int $capitulo
 * @property int $versiculo
 * @property string $texto
 *
 * @property \App\Model\Entity\Version $version
 * @property \App\Model\Entity\Livro $livro
 */
class Versiculo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'version_id' => true,
        'livro_id' => true,
        'capitulo' => true,
        'versiculo' => true,
        'texto' => true,
        'version' => true,
        'livro' => true
    ];
}
