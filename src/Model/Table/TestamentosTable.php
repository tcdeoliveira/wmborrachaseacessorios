<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Testamentos Model
 *
 * @property \App\Model\Table\LivrosTable|\Cake\ORM\Association\HasMany $Livros
 *
 * @method \App\Model\Entity\Testamento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Testamento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Testamento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Testamento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Testamento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Testamento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Testamento findOrCreate($search, callable $callback = null, $options = [])
 */
class TestamentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('testamentos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Livros', [
            'foreignKey' => 'testamento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 30)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'biblia';
    }
}
