<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Congregados Model
 *
 * @method \App\Model\Entity\Congregado get($primaryKey, $options = [])
 * @method \App\Model\Entity\Congregado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Congregado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Congregado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Congregado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Congregado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Congregado findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CongregadosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('congregados');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('sobrenome')
            ->maxLength('sobrenome', 255)
            ->requirePresence('sobrenome', 'create')
            ->notEmpty('sobrenome');

        $validator
            ->scalar('foto')
            ->maxLength('foto', 255)
            ->allowEmpty('foto');

        $validator
            ->scalar('apelido')
            ->maxLength('apelido', 255)
            ->allowEmpty('apelido');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->date('data_nsc')
            ->requirePresence('data_nsc', 'create')
            ->notEmpty('data_nsc');

        $validator
            ->scalar('ano')
            ->requirePresence('ano', 'create')
            ->notEmpty('ano');

        $validator
            ->integer('sexo')
            ->requirePresence('sexo', 'create')
            ->notEmpty('sexo');

        $validator
            ->integer('tipo')
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->integer('regra')
            ->requirePresence('regra', 'create')
            ->notEmpty('regra');

        $validator
            ->scalar('telefone1_ddd')
            ->maxLength('telefone1_ddd', 255)
            ->allowEmpty('telefone1_ddd');

        $validator
            ->scalar('telefone1_numero')
            ->maxLength('telefone1_numero', 255)
            ->allowEmpty('telefone1_numero');

        $validator
            ->scalar('telefone1_tipo')
            ->maxLength('telefone1_tipo', 255)
            ->allowEmpty('telefone1_tipo');

        $validator
            ->scalar('telefone2_ddd')
            ->maxLength('telefone2_ddd', 255)
            ->allowEmpty('telefone2_ddd');

        $validator
            ->scalar('telefone2_numero')
            ->maxLength('telefone2_numero', 255)
            ->allowEmpty('telefone2_numero');

        $validator
            ->scalar('telefone2_tipo')
            ->maxLength('telefone2_tipo', 255)
            ->allowEmpty('telefone2_tipo');

        $validator
            ->scalar('cep')
            ->maxLength('cep', 255)
            ->allowEmpty('cep');

        $validator
            ->scalar('rua')
            ->maxLength('rua', 255)
            ->allowEmpty('rua');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 255)
            ->allowEmpty('numero');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro', 255)
            ->allowEmpty('bairro');

        $validator
            ->scalar('cidade')
            ->maxLength('cidade', 255)
            ->allowEmpty('cidade');

        $validator
            ->scalar('estado')
            ->maxLength('estado', 255)
            ->allowEmpty('estado');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento', 255)
            ->allowEmpty('complemento');

        $validator
            ->scalar('mapa')
            ->allowEmpty('mapa');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
