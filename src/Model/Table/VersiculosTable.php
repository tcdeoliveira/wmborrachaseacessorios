<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Versiculos Model
 *
 * @property \App\Model\Table\VersionsTable|\Cake\ORM\Association\BelongsTo $Versions
 * @property \App\Model\Table\LivrosTable|\Cake\ORM\Association\BelongsTo $Livros
 *
 * @method \App\Model\Entity\Versiculo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Versiculo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Versiculo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Versiculo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Versiculo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Versiculo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Versiculo findOrCreate($search, callable $callback = null, $options = [])
 */
class VersiculosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('versiculos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Versions', [
            'foreignKey' => 'version_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Livros', [
            'foreignKey' => 'livro_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('capitulo', 'create')
            ->notEmpty('capitulo');

        $validator
            ->requirePresence('versiculo', 'create')
            ->notEmpty('versiculo');

        $validator
            ->scalar('texto')
            ->requirePresence('texto', 'create')
            ->notEmpty('texto');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['version_id'], 'Versions'));
        $rules->add($rules->existsIn(['livro_id'], 'Livros'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'biblia';
    }
}
