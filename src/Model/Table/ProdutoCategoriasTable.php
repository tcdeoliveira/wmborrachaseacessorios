<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProdutoCategorias Model
 *
 * @property \App\Model\Table\ProdutosTable|\Cake\ORM\Association\HasMany $Produtos
 *
 * @method \App\Model\Entity\ProdutoCategoria get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProdutoCategoria newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProdutoCategoria[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProdutoCategoria|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProdutoCategoria patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProdutoCategoria[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProdutoCategoria findOrCreate($search, callable $callback = null, $options = [])
 */
class ProdutoCategoriasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('produto_categorias');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');

        $this->hasMany('Produtos', [
            'foreignKey' => 'produto_categoria_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
