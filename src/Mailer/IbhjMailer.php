<?php
namespace App\Mailer;
use Cake\Mailer\Mailer;
class IbhjMailer extends Mailer
{
    static public $name = 'User';
    public function novocadastro($cadastro){
    	$this->to("comunicacao@ibhj.com.br")
        ->addTo('tiago@ibhj.com.br')    
        ->transport('ibhj')
        ->from ( ['cadastro@ibhj.com.br' => 'IBHJ | Cadastro de Congregados'])
    	->emailFormat('html')
    	->template('novocadastro')
        ->viewVars(['cad'=>$cadastro])
    	->subject(sprintf($cadastro['nome'].' cadastrou-se no site'));
    }
    public function contato($email){
        $this->to("tcdeoliveira@outlook.com")
        ->transport('wm')
        ->from ( [$email['email'] => $email['nome']])
        ->emailFormat('html')
        ->template('ibhj')
        ->viewVars(['nome'=>$email['nome'], 'menssagem'=>$email['menssagem'], 'email'=>$email['email']])
        ->subject(sprintf('Contato'));
    }    
}
