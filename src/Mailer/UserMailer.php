<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;

/**
 * User mailer.
 */
class UserMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'User';
    public function welcome(){
    	$this->to("emailtiagodeoliveira@gmail.com")
        ->transport('ibhj')
        ->from ( ['comunicacao@ibhj.com.br' => 'Igreja Btista de Henrique Jorge'])
    	->emailFormat('html')
    	->template('ibhj')
    	->subject(sprintf('Bem vindo'));
    }
    public function pedidodeoracao($pedido){
    	$this->to("emailtiagodeoliveira@gmail.com")
        ->transport('ibhj')
        ->from ( ['comunicacao@ibhj.com.br' => 'Igreja Btista de Henrique Jorge'])
    	->emailFormat('html')
    	->template('pedidodeoracao','user')    	
        ->viewVars(['nome'=>$pedido->nome, 'pedido'=>$pedido->pedido])
        ->subject(sprintf('Pedido de oração'));
    }    
    public function senha($user){
    	$this->to($user->email)
        ->transport('ibhj')
        ->from ( ['comunicacao@ibhj.com.br' => 'Igreja Btista de Henrique Jorge'])
    	->emailFormat('html')
    	->template('senha','user')    	
        ->viewVars(['nome'=>$user->nome, 'senha'=>$user->newpass])
        ->subject(sprintf('Redefinição de senha'));
        
    }    
    public function desativar(){
    	$this->to("emailtiagodeoliveira@gmail.com")
        ->transport('ibhj')
        ->from ( ['comunicacao@ibhj.com.br' => 'Comunicação Ibhj'])
    	->emailFormat('html')
    	->template('desativar','user')
    	->subject(sprintf('Bem vindo'));
    }
    
    }
